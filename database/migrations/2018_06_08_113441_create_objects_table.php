<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->increments('id', 10)->unsigned();
            $table->integer('micro_district_id')->unsigned()->index();
            $table->integer('deal_type_id')->unsigned()->index();
            $table->integer('object_type_id')->unsigned()->index();
            $table->text('name')->nullable();//number of storeys
            $table->integer('number_of_storeys')->nullable()->index();
            $table->enum('type', ['panel', 'brick','monolithic'])->index();
            $table->timestamps();

            $table->foreign('micro_district_id')->references('micro_district_id')->on('microdistricts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
