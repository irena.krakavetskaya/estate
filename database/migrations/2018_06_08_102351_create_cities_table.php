<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id', 10)->unsigned();
            $table->string('city',255)->index();
            $table->integer('city_id')->unsigned()->index();
            $table->text('region')->nullable();
            $table->string('translit',255)->index()->nullable();
            $table->text('roditelniy')->nullable();
            $table->text('predlozhniy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
