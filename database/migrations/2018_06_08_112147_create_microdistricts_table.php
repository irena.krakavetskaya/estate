<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMicrodistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microdistricts', function (Blueprint $table) {
            $table->increments('id', 10)->unsigned();
            $table->integer('district_id')->unsigned()->index();
            $table->integer('micro_district_id')->unsigned()->index();
            $table->text('micro_district_name')->nullable();
            $table->string('translit',255)->index()->nullable();
            $table->text('roditelniy')->nullable();
            $table->text('predlozhniy')->nullable();
            $table->timestamps();

            $table->foreign('district_id')->references('district_id')->on('districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microdistricts');
    }
}
