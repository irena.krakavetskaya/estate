<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevelopersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developers', function (Blueprint $table) {
            $table->increments('id', 10)->unsigned();
            $table->integer('dev_id')->unsigned()->index();
            $table->string('name',191)->index();
            $table->text('legal')->nullable();
            $table->longText('html')->nullable();
            $table->text('web')->nullable();
            $table->text('email')->nullable();
            $table->text('address')->nullable();
            $table->text('logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developers');
    }
}
