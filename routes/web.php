<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('auth.login');
});
*/
//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

//Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('cities', 'CityController');
    Route::view('cities/browse/file', 'cities.upload'); //only in laravel 5.5
    Route::post('cities/upload', ['uses' => 'CityController@upload']);
    Route::post('cities/download', ['uses' => 'CityController@download']);

    Route::resource('districts', 'DistrictController');
    Route::view('districts/browse/file', 'districts.upload');
    Route::post('districts/upload', ['uses' => 'DistrictController@upload']);
    Route::post('districts/download', ['uses' => 'DistrictController@download']);

    Route::resource('microDistricts', 'MicroDistrictController');
    Route::view('microDistricts/browse/file', 'micro_districts.upload');
    Route::post('microDistricts/upload', ['uses' => 'MicroDistrictController@upload']);
    Route::post('microDistricts/download', ['uses' => 'MicroDistrictController@download']);

    //Route::group(['middleware' => ['role:admin']], function () {
        Route::resource('dealTypes', 'DealTypeController');

        Route::resource('objectTypes', 'ObjectTypeController');
    //});

    Route::resource('objects', 'ObjectController');
    Route::view('objects/browse/file', 'objects.upload');
    Route::post('objects/upload', ['uses' => 'ObjectController@upload']);
    Route::post('objects/download', ['uses' => 'ObjectController@download']);


    Route::resource('roles', 'RoleController');

    Route::resource('users', 'UserController');
});





Route::resource('logs', 'LogController');

Route::resource('developers', 'DeveloperController');