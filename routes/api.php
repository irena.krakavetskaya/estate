<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group(['prefix' => 'v1/'], function () {
    Route::resource('cities', 'CityAPIController');

    Route::resource('districts', 'DistrictAPIController');

    Route::resource('micro_districts', 'MicroDistrictAPIController');

    Route::resource('deal_types', 'DealTypeAPIController');

    Route::resource('object_types', 'ObjectTypeAPIController');

    Route::resource('objects', 'ObjectAPIController');
});

Route::resource('users', 'UserAPIController');

Route::resource('logs', 'LogAPIController');

Route::resource('developers', 'DeveloperAPIController');