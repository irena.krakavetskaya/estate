<?php
namespace App\Traits;

use App\User;

trait LogTrait
{
    public function logActivity($model,$action,$old='')
    {
        date_default_timezone_set('Europe/Moscow');
        activity()
            ->performedOn($model)
            ->withProperties([
                $old,$model
            ])
            ->inLog($action)
            ->log('');
        /*
        if($action=='update'){
            activity()
                ->performedOn($model)
                ->withProperties([
                    $old,$model
                ])
                ->log($action);
        }
        elseif($action=='delete'){
            activity()
                ->performedOn($model)
                ->withProperties([
                    $model
                ])
                ->log($action);
        }
        else{
            activity()
                ->performedOn($model)
                ->log($action);
        }
        */
    }

    public function parseLog($log)
    {
        $prop = [];
        $properties = \GuzzleHttp\json_decode($log->properties, true);
        if(!empty($properties)){
            $unused = ['id','created_at','updated_at'];
            foreach($properties as $key=>$value) {
                if (isset($key) && $value!==""){
                    foreach ($value as $k => $v) {
                        if (in_array($k, $unused)) {
                            continue;
                        } else {
                            if ($key == 0) {
                                $prop[$k][0] = $v;
                            } else {
                                $prop[$k][1] = $v;
                            }

                        }
                    }
                }
            }
        }

        $str = $log->subject_type;
        $str = '\\' . $str;
        $model = new $str();
        $subject = $model->translation;

        $subject_table = $model->table;

        $email = User::where('id', $log->causer_id)->value('email');

        $result=[
            'prop'=>$prop,
            'subject'=>$subject,
            'subject_table'=>$subject_table,
            'email'=>$email
        ];

        return $result;
    }
}