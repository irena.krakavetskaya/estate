<?php
namespace App\Traits;

use Illuminate\Http\Request;
use Flash;
use Excel;
trait ExcelTrait{
    public function uploadFile($file, $table_name, $model, $foreign_key, $arr, $required)
    {
        $messages = [
            0 => 'Файл не загружен',
            1 => 'Размер файла не должен превышать 6 Мб',
            2 => 'Файл должен быть в формате xls, xlsx или csv',
            3 => 'Файл содержит некорректные данные',
            4 => 'Содержимое данного файла уже существует в бд',
            5 => 'Файл пустой',
            6 => 'Информация из файла успешно загружена',
        ];

        if (file_exists($file)) {
            $info = new \SplFileInfo($_FILES[$table_name]['name']); //'cities'
            $url = $table_name . '/browse/file';


            $size = $file->getSize();
            if ($size > 6000000) { //1 048 576 b - 1 mb
                Flash::error($messages[1]);
                return redirect(url($url));
            }
            if ($size < 1) {
                Flash::error($messages[5]);
                return redirect(url($url));
            }
            $correct_ext = ['xls', 'xlsx', 'csv'];
            $ext = $info->getExtension();
            if (in_array($ext, $correct_ext) === false) {
                Flash::error($messages[2]);
                return redirect(url($url));
            }

            $data1 = Excel::load($file)->get();
            $data = [];
            foreach ($data1 as $rowObj) {
                $is_row_empty = true;
                foreach ($rowObj as $cell) {
                    if ($cell !== '' && $cell !== null) {
                        $is_row_empty = false; //detect not empty row
                        break;
                    }
                }
                if ($is_row_empty) { // skip empty row
                    continue;
                }
                array_push($data, $rowObj);//$rowObj->toArray()
            }


            foreach ($data as $key => $value) {
                foreach ($required as $num => $val) {
                    if (!isset($value[$val])) {
                        Flash::error($messages[3]);
                        return redirect(url($url));
                    };
                }

                foreach ($foreign_key as $num => $val) {
                    if (!isset($value[$val])) {
                        Flash::error($messages[3]);
                        return redirect(url($url));
                    };
                }
            }

            \DB::statement('SET FOREIGN_KEY_CHECKS=0');
            $model->truncate();
            \DB::statement('SET FOREIGN_KEY_CHECKS = 1');


            foreach ($data as $key => $value) {
                $insert = array();
                foreach ($arr as $k => $v) {
                    $insert[$v] = $value[$v];
                }
                $model->insert([
                    $insert
                ]);
            }


            Flash::success($messages[6]);
            return redirect(url($this->convertToCamelCase($table_name)));
        }
        else {
            $url = $table_name . '/browse/file';
            Flash::error($messages[0]);
            return redirect(url($url));
        }
    }

    public function downloadFile($model, $filename)
    {
        $model =  $model->each(function ($item) {
            unset($item['id'],$item['created_at'],$item['updated_at']);
        });
        $res= $model->toArray();

        Excel::create($filename, function ($excel) use ($res) {
            $excel->sheet('List', function ($sheet) use ($res) {
                $sheet->fromArray($res, null, 'A1', true);
            });
        })->download('xls');
    }

    public function convertToCamelCase($table_name)
    {
        //transform snake_case to camelCase
        $pos = strpos($table_name, '_');
        if ($pos > 0) {
            $table_name[++$pos] = strtoupper($table_name[$pos]);
            $table_name = str_replace('_', '', $table_name);
        }
        $pos1 = strpos($table_name, '2');
        if ($pos1 > 0) {
            $table_name[++$pos1] = strtoupper($table_name[$pos1]);
        }
        return $table_name;
    }
}