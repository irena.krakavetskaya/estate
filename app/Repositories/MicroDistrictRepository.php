<?php

namespace App\Repositories;

use App\Models\District;
use App\Models\MicroDistrict;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MicroDistrictRepository
 * @package App\Repositories
 * @version June 8, 2018, 12:43 pm UTC
 *
 * @method MicroDistrict findWithoutFail($id, $columns = ['*'])
 * @method MicroDistrict find($id, $columns = ['*'])
 * @method MicroDistrict first($columns = ['*'])
*/
class MicroDistrictRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'district_id',
        'micro_district_id',
        'name',
        'translit',
        'roditelniy',
        'predlozhniy'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MicroDistrict::class;
    }

    public function getDistricts()
    {
        return District::orderBy('name', 'asc')->pluck('name', 'district_id');
    }

    public function getMicrodistricts()
    {
        $microdistricts = $this->model->get();
        return $microdistricts->each(function ($item) {
            unset($item['id']);
        });
    }

    public function getModel()
    {
        return $this->model;
    }
}
