<?php

namespace App\Repositories;

use App\Models\ObjectType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ObjectTypeRepository
 * @package App\Repositories
 * @version June 8, 2018, 12:49 pm UTC
 *
 * @method ObjectType findWithoutFail($id, $columns = ['*'])
 * @method ObjectType find($id, $columns = ['*'])
 * @method ObjectType first($columns = ['*'])
*/
class ObjectTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'object_type',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ObjectType::class;
    }
}
