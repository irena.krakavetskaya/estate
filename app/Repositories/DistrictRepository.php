<?php

namespace App\Repositories;

use App\Models\City;
use App\Models\District;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DistrictRepository
 * @package App\Repositories
 * @version June 8, 2018, 12:42 pm UTC
 *
 * @method District findWithoutFail($id, $columns = ['*'])
 * @method District find($id, $columns = ['*'])
 * @method District first($columns = ['*'])
*/
class DistrictRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city_id',
        'name',
        'district_id',
        'translit'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return District::class;
    }

    public function getCities()
    {
        return City::orderBy('city', 'asc')->pluck('city', 'city_id');
    }

    public function getDistricts()
    {
        $districts = $this->model->get();
        return $districts->each(function ($item) {
            unset($item['id']);
        });
    }

    public function getModel()
    {
        return $this->model;
    }
}
