<?php

namespace App\Repositories;

use App\Models\DealType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DealTypeRepository
 * @package App\Repositories
 * @version June 8, 2018, 12:49 pm UTC
 *
 * @method DealType findWithoutFail($id, $columns = ['*'])
 * @method DealType find($id, $columns = ['*'])
 * @method DealType first($columns = ['*'])
*/
class DealTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'deal_type',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DealType::class;
    }
}
