<?php

namespace App\Repositories;

use App\Models\DealType;
use App\Models\MicroDistrict;
use App\Models\Object;
use App\Models\ObjectType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ObjectRepository
 * @package App\Repositories
 * @version June 8, 2018, 12:49 pm UTC
 *
 * @method Object findWithoutFail($id, $columns = ['*'])
 * @method Object find($id, $columns = ['*'])
 * @method Object first($columns = ['*'])
*/
class ObjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'micro_district_id',
        'deal_type_id',
        'object_type_id',
        'name',
        'number_of_storeys',
        'type'
    ];

    public $types =['panel'=>'Панельный','brick'=>'Кирпичный', 'monolithic'=>'Монолитный'];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Object::class;
    }

    public function getMicroDistricts()
    {
        return MicroDistrict::orderBy('name', 'asc')->pluck('name', 'micro_district_id');
    }

    public function getDealTypes()
    {
        return DealType::orderBy('name', 'asc')->pluck('name', 'id');
    }

    public function getObjectTypes()
    {
        return ObjectType::orderBy('name', 'asc')->pluck('name', 'id');
    }

    public function getObjects()
    {
        $objects = $this->model->get();
        return $objects->each(function ($item) {
            unset($item['id']);
        });
    }

    public function getModel()
    {
        return $this->model;
    }
}
