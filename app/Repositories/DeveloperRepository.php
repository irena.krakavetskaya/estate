<?php

namespace App\Repositories;

use App\Models\Developer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DeveloperRepository
 * @package App\Repositories
 * @version June 13, 2018, 2:58 pm UTC
 *
 * @method Developer findWithoutFail($id, $columns = ['*'])
 * @method Developer find($id, $columns = ['*'])
 * @method Developer first($columns = ['*'])
*/
class DeveloperRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dev_id',
        'name',
        'legal',
        'html',
        'web',
        'email',
        'address',
        'logo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Developer::class;
    }
}
