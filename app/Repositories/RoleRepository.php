<?php

namespace App\Repositories;


use InfyOm\Generator\Common\BaseRepository;
use Spatie\Permission\Models\Role;


class RoleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'guard_name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Role::class;
    }


}
