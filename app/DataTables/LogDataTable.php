<?php

namespace App\DataTables;


use App\Models\Log;
use App\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;


class LogDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('created_at', function (Log $log) {
                return $log->created_at->format('d-m-Y H:i:s');
            })
            ->editColumn('subject_type', function (Log $log) {
                $str = $log->subject_type;
                $str =  '\\'.$str;
                $model = new $str();
                return $model->translation;
                //$id = $model->where('id', $log->subject_id)->value('id');
            })
            ->editColumn('subject_id', function (Log $log) {
                $str = $log->subject_type;
                $str =  '\\'.$str;
                $model = new $str();
                return '<a href="/'.$model->table.'/' . $log->subject_id . '/edit" target="_blank">' . $log->subject_id . '</a>';//? if city has been deleted

            })
            ->editColumn('causer_id', function (Log $log) {
                $email = User::where('id',$log->causer_id )->value('email');
                return '<a href="/users/' . $log->causer_id . '/edit" target="_blank">' .  $email . '</a>';
            })
            ->rawColumns(['subject_id','action','causer_id'])
            ->addColumn('action', 'logs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Log $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'fixedHeader'=>true,
                'pageLength' => 30,
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            //'id',
            'created_at'=>['title'=>'Дата и время'],
            'log_name'=>['title'=>'Тип изменения'],
            //'description',
            'subject_type'=>['title'=>'Тип субъекта изменения'],
            'subject_id'=>['title'=>'ID субъекта'],
            'causer_id'=>['title'=>'Кто изменил'],

            //'causer_type',
            //'properties'

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'logsdatatable_' . time();
    }
}