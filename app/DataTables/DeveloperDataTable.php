<?php

namespace App\DataTables;

use App\Models\Developer;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class DeveloperDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('web', function (Developer $developer) {
                return '<a href="'.$developer->web.'" target="_blank">' . $developer->web . '</a>';
            })
            ->rawColumns(['web','action'])
            ->addColumn('action', 'developers.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Developer $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'fixedHeader'=>true,
                'pageLength' => 30,
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'dev_id',
            'name'=>['title'=>'Название'],
            'legal'=>['title'=>'Юр. название'],
            //'html',
            'web'=>['title'=>'Сайт'],
            'email'=>['title'=>'Эл. почта'],
            'address'=>['title'=>'Адрес'],
            'logo'=>['title'=>'Логотип'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'developersdatatable_' . time();
    }
}