<?php

namespace App\DataTables;

use App\Models\MicroDistrict;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class MicroDistrictDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('city_id', function (MicroDistrict $microdistrict) {
                return
                    '<a href="/'.$microdistrict->districtMicrodistrict->cityDistrict->table.'/'
                    . $microdistrict->districtMicrodistrict->cityDistrict->id . '/edit">'
                    . $microdistrict->districtMicrodistrict->cityDistrict->city . '</a>';
            })
            ->editColumn('district_id', function (MicroDistrict $microdistrict) {
                return
                '<a href="/'.$microdistrict->districtMicrodistrict->table.'/' . $microdistrict->districtMicrodistrict->id . '/edit">' . $microdistrict->districtMicrodistrict->name . '</a>';
            })
            ->rawColumns(['city_id','district_id','action'])
            ->addColumn('action', 'micro_districts.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MicroDistrict $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'fixedHeader'=>true,
                'pageLength' => 30,
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'city_id'=>['name' => 'city_id', 'data' => 'city_id','title'=>'Город'],
            'district_id'=>['name' => 'district_id', 'data' => 'district_id','title'=>'Район'],
            'name',
            'micro_district_id',
            'translit',
            'roditelniy',
            'predlozhniy'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'micro_districtsdatatable_' . time();
    }
}