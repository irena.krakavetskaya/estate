<?php

namespace App\DataTables;

use App\Models\Object;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ObjectDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('city_id', function (Object $object) {
                return
                    '<a href="/'.$object->microdistrictObject->districtMicrodistrict->cityDistrict->table.'/'
                    . $object->microdistrictObject->districtMicrodistrict->cityDistrict->id . '/edit">'
                    . $object->microdistrictObject->districtMicrodistrict->cityDistrict->city . '</a>';
            })
            ->editColumn('district_id', function (Object $object) {
                return
                    '<a href="/'.$object->microdistrictObject->districtMicrodistrict->table.'/'
                    . $object->microdistrictObject->districtMicrodistrict->id . '/edit">'
                    . $object->microdistrictObject->districtMicrodistrict->name . '</a>';
            })

            ->editColumn('micro_district_id', function (Object $object) {
                return
                    '<a href="/microDistricts/' . $object->microdistrictObject->id . '/edit">' . $object->microdistrictObject->name . '</a>';
            })
            ->editColumn('deal_type_id', function (Object $object) {
                return
                    '<a href="/dealTypes/' . $object->dealTypeObject->id . '/edit">' . $object->dealTypeObject->name . '</a>';
            })
            ->editColumn('object_type_id', function (Object $object) {
                return
                    '<a href="/objectTypes/' . $object->objectTypeObject->id . '/edit">' . $object->objectTypeObject->name . '</a>';
            })
            ->editColumn('type', function (Object $object) {
                return  $object->getType();
            })
             ->rawColumns(['city_id','district_id','micro_district_id','deal_type_id','object_type_id', 'action'])
             ->addColumn('action', 'objects.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Object $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'fixedHeader'=>true,
                'pageLength' => 30,
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'city_id'=>['name' => 'city_id', 'data' => 'city_id','title'=>'Город'],
            'district_id'=>['name' => 'district_id', 'data' => 'district_id','title'=>'Район'],
            'micro_district_id'=>['name' => 'micro_district_id', 'data' => 'micro_district_id','title'=>'Микрорайон'],
            'deal_type_id'=>['name' => 'deal_type_id', 'data' => 'deal_type_id','title'=>'Тип сделки'],
            'object_type_id'=>['name' => 'object_type_id', 'data' => 'object_type_id','title'=>'Тип объекта'],
            'name'=>['name' => 'name', 'data' => 'name','title'=>'Название'],
            'number_of_storeys'=>['name' => 'number_of_storeys', 'data' => 'number_of_storeys','title'=>'Этажность'],
            'type'=>['name' => 'type', 'data' => 'type','title'=>'Тип']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'objectsdatatable_' . time();
    }
}