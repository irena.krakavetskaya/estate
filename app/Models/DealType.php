<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class DealType
 * @package App\Models
 * @version June 8, 2018, 12:49 pm UTC
 *
 * @property string deal_type
 * @property string name
 */
class DealType extends Model
{

    public $table = 'deal_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'deal_type',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deal_type' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'deal_type' => 'required',
    ];

    public function dealTypeObjects()
    {
        return $this->hasMany(\App\Models\Object::class, 'deal_type_id', 'id');
    }
}
