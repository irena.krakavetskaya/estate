<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class City
 * @package App\Models
 * @version June 8, 2018, 11:48 am UTC
 *
 * @property string city
 * @property integer city_id
 * @property string region
 * @property string translit
 * @property string roditelniy
 * @property string predlozhniy
 */
class City extends Model
{

    public $table = 'cities';
    public $translation = 'Города';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'city',
        'city_id',
        'region',
        'translit',
        'roditelniy',
        'predlozhniy'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'city' => 'string',
        'city_id' => 'integer',
        'region' => 'string',
        'translit' => 'string',
        'roditelniy' => 'string',
        'predlozhniy' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'city' => 'required',
        'city_id' => 'required',
    ];

    public function cityDistrict()
    {
        return $this->hasMany(\App\Models\District::class, 'city_id', 'city_id');
    }
}
