<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class MicroDistrict
 * @package App\Models
 * @version June 8, 2018, 12:43 pm UTC
 *
 * @property integer district_id
 * @property integer micro_district_id
 * @property string name
 * @property string translit
 * @property string roditelniy
 * @property string predlozhniy
 */
class MicroDistrict extends Model
{

    public $table = 'microdistricts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'district_id',
        'micro_district_id',
        'name',
        'translit',
        'roditelniy',
        'predlozhniy'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'district_id' => 'integer',
        'micro_district_id' => 'integer',
        'name' => 'string',
        'translit' => 'string',
        'roditelniy' => 'string',
        'predlozhniy' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'micro_district_id' => 'required',
    ];

    public function microdistrictObjects()
    {
        return $this->hasMany(\App\Models\Object::class, 'micro_district_id', 'micro_district_id');
    }

    public function districtMicrodistrict()
    {
        return $this->belongsTo(\App\Models\District::class, 'district_id', 'district_id');
    }
    
}
