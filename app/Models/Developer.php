<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Developer
 * @package App\Models
 * @version June 13, 2018, 2:58 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection roleHasPermissions
 * @property integer dev_id
 * @property string name
 * @property string legal
 * @property string html
 * @property string web
 * @property string email
 * @property string address
 * @property string logo
 */
class Developer extends Model
{

    public $table = 'developers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'dev_id',
        'name',
        'legal',
        'html',
        'web',
        'email',
        'address',
        'logo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'dev_id' => 'integer',
        'name' => 'string',
        'legal' => 'string',
        'html' => 'string',
        'web' => 'string',
        'email' => 'string',
        'address' => 'string',
        'logo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
