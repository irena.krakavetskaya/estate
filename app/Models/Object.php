<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Object
 * @package App\Models
 * @version June 8, 2018, 12:49 pm UTC
 *
 * @property integer micro_district_id
 * @property integer deal_type_id
 * @property integer object_type_id
 * @property string name
 * @property integer number_of_storeys
 * @property string type
 */
class Object extends Model
{

    public $table = 'objects';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'micro_district_id',
        'deal_type_id',
        'object_type_id',
        'name',
        'number_of_storeys',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'micro_district_id' => 'integer',
        'deal_type_id' => 'integer',
        'object_type_id' => 'integer',
        'name' => 'string',
        'number_of_storeys' => 'integer',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function microdistrictObject()
    {
        return $this->belongsTo(\App\Models\MicroDistrict::class, 'micro_district_id', 'micro_district_id');
    }

    public function dealTypeObject()
    {
        return $this->belongsTo(\App\Models\DealType::class, 'deal_type_id', 'id');
    }

    public function objectTypeObject()
    {
        return $this->belongsTo(\App\Models\ObjectType::class, 'object_type_id', 'id');
    }

    public function getType()
    {
        switch ($this->type) {
            case('panel'):return 'Панельный';
            case('brick'):return 'Кирпичный';
            case('monolithic'):return 'Монолитный';
        }
    }

}
