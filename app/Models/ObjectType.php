<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ObjectType
 * @package App\Models
 * @version June 8, 2018, 12:49 pm UTC
 *
 * @property string object_type
 * @property string name
 */
class ObjectType extends Model
{

    public $table = 'object_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'object_type',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'object_type' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'object_type' => 'required',
        'name' => 'required'
    ];

    public function objectTypeObjects()
    {
        return $this->hasMany(\App\Models\Object::class, 'object_type_id', 'id');
    }
    
}
