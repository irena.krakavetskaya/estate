<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class District
 * @package App\Models
 * @version June 8, 2018, 12:42 pm UTC
 *
 * @property integer city_id
 * @property string name
 * @property integer district_id
 * @property string translit
 */
class District extends Model
{

    public $table = 'districts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'city_id',
        'name',
        'district_id',
        'translit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'city_id' => 'integer',
        'name' => 'string',
        'district_id' => 'integer',
        'translit' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'district_id' => 'required',
    ];

    public function districtMicrodistricts()
    {
        return $this->hasMany(\App\Models\MicroDistrict::class, 'district_id', 'district_id');
    }

    public function cityDistrict()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'city_id');
    }
}
