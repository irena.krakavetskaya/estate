<?php

namespace App\Http\Controllers;

use App\DataTables\DistrictDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDistrictRequest;
use App\Http\Requests\UpdateDistrictRequest;
use App\Models\District;
use App\Repositories\DistrictRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;

class DistrictController extends AppBaseController
{
    /** @var  DistrictRepository */
    private $districtRepository;

    public function __construct(DistrictRepository $districtRepo)
    {
        $this->districtRepository = $districtRepo;
    }

    /**
     * Display a listing of the District.
     *
     * @param DistrictDataTable $districtDataTable
     * @return Response
     */
    public function index(DistrictDataTable $districtDataTable)
    {
        return $districtDataTable->render('districts.index');
    }

    /**
     * Show the form for creating a new District.
     *
     * @return Response
     */
    public function create()
    {

        $cities = $this->districtRepository->getCities();
        return view('districts.create',compact('cities'));
    }

    /**
     * Store a newly created District in storage.
     *
     * @param CreateDistrictRequest $request
     *
     * @return Response
     */
    public function store(CreateDistrictRequest $request)
    {
        $input = $request->all();

        $district = $this->districtRepository->create($input);

        Flash::success('District saved successfully.');

        return redirect(route('districts.index'));
    }

    /**
     * Display the specified District.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $district = $this->districtRepository->findWithoutFail($id);

        if (empty($district)) {
            Flash::error('District not found');

            return redirect(route('districts.index'));
        }

        return view('districts.show')->with('district', $district);
    }

    /**
     * Show the form for editing the specified District.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $district = $this->districtRepository->findWithoutFail($id);

        if (empty($district)) {
            Flash::error('District not found');

            return redirect(route('districts.index'));
        }

        $cities = $this->districtRepository->getCities();


        //return view('districts.edit')->with('district', $district);
        return view('districts.edit',compact('district','cities'));
    }

    /**
     * Update the specified District in storage.
     *
     * @param  int              $id
     * @param UpdateDistrictRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDistrictRequest $request)
    {
        $district = $this->districtRepository->findWithoutFail($id);

        if (empty($district)) {
            Flash::error('District not found');

            return redirect(route('districts.index'));
        }

        $district = $this->districtRepository->update($request->all(), $id);

        Flash::success('District updated successfully.');

        return redirect(route('districts.index'));
    }

    /**
     * Remove the specified District from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $district = $this->districtRepository->findWithoutFail($id);

        if (empty($district)) {
            Flash::error('District not found');
            return redirect(route('districts.index'));
        }

        $relatedMicrodistricts =District::has('districtMicrodistricts')->where('id',$id)->count();
        if($relatedMicrodistricts){
            Flash::error('Сначала удалите  микрорайоны, связанные с этим районом');
            return redirect(route('districts.index'));
        }
        else {
            $this->districtRepository->delete($id);
            Flash::success('District deleted successfully.');
            return redirect(route('districts.index'));
        }

    }

    public function upload(Request $request)
    {
        $file = $request->file('districts');

        $districts = $this->districtRepository->getModel();//$city=new City();
        $arr=$districts->fillable;

        $required=['name','district_id'];
        $foreign_key=['city_id'];

        return $this->uploadFile($file, 'districts', $districts, $foreign_key, $arr, $required);
    }

    public function download()
    {
        $districts=District::get();

        return $this->downloadFile($districts,  'District');
    }
}
