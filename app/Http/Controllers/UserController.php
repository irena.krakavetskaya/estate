<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {

        return $userDataTable->render('users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        //return view('users.create');
        //$roles= Role::pluck('name', 'id')->toArray();
        $roles= Role::pluck('name','name');//->toArray();


        return view('users.create')->with('roles',$roles);

    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {

        Validator::make($request->all(), [
            'email' =>[ 'required', Rule::unique('users')],
            'password' =>['required'],
        ])->validate();

        $role= $request->roles;
        $input = $request->all();
        $input['password']=Hash::make($request->password);

        $user = $this->userRepository->create($input);
        $user->assignRole($role);

        Flash::success('User saved successfully.');
        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }

        $user = $this->userRepository->findWithoutFail($id);
        $roles= Role::pluck('name', 'name');
        $role = $user->getRoleNames()->toArray();
        //var_dump($role);

        return view('users.edit',compact('user', 'roles','role'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }


        Validator::make($request->all(), [
            'email' =>[ 'required', Rule::unique('users')->ignore($user->id)],
        ])->validate();


        $role= $request->roles;
        $user = $this->userRepository->update($request->all(), $id);

        $roles = $user->getRoleNames()->toArray();
        if($roles!=[]){
            $user->removeRole($roles[0]);
        }
        $user->assignRole($role);



        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);





        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
