<?php

namespace App\Http\Controllers;

use App\DataTables\ObjectDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateObjectRequest;
use App\Http\Requests\UpdateObjectRequest;
use App\Repositories\ObjectRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ObjectController extends AppBaseController
{
    /** @var  ObjectRepository */
    private $objectRepository;

    public function __construct(ObjectRepository $objectRepo)
    {
        $this->objectRepository = $objectRepo;
    }

    /**
     * Display a listing of the Object.
     *
     * @param ObjectDataTable $objectDataTable
     * @return Response
     */
    public function index(ObjectDataTable $objectDataTable)
    {
        return $objectDataTable->render('objects.index');
    }

    /**
     * Show the form for creating a new Object.
     *
     * @return Response
     */
    public function create()
    {
        //return view('objects.create');
        $microdistricts = $this->objectRepository->getMicroDistricts();
        $dealTypes = $this->objectRepository->getDealTypes();
        $objectTypes = $this->objectRepository->getObjectTypes();

        $types = $this->objectRepository->types;

        return view('objects.create',compact('microdistricts','dealTypes','objectTypes' ,'types'));
    }

    /**
     * Store a newly created Object in storage.
     *
     * @param CreateObjectRequest $request
     *
     * @return Response
     */
    public function store(CreateObjectRequest $request)
    {
        $input = $request->all();

        $object = $this->objectRepository->create($input);

        Flash::success('Object saved successfully.');

        return redirect(route('objects.index'));
    }

    /**
     * Display the specified Object.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            Flash::error('Object not found');

            return redirect(route('objects.index'));
        }

        return view('objects.show')->with('object', $object);
    }

    /**
     * Show the form for editing the specified Object.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            Flash::error('Object not found');
            return redirect(route('objects.index'));
        }

        $microdistricts = $this->objectRepository->getMicroDistricts();
        $dealTypes = $this->objectRepository->getDealTypes();
        $objectTypes = $this->objectRepository->getObjectTypes();

        $types = $this->objectRepository->types;

        return view('objects.edit',compact('object','microdistricts','dealTypes','objectTypes' ,'types'));
        //return view('objects.edit')->with('object', $object);
    }

    /**
     * Update the specified Object in storage.
     *
     * @param  int              $id
     * @param UpdateObjectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjectRequest $request)
    {
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            Flash::error('Object not found');

            return redirect(route('objects.index'));
        }

        $object = $this->objectRepository->update($request->all(), $id);

        Flash::success('Object updated successfully.');

        return redirect(route('objects.index'));
    }

    /**
     * Remove the specified Object from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            Flash::error('Object not found');

            return redirect(route('objects.index'));
        }

        $this->objectRepository->delete($id);

        Flash::success('Object deleted successfully.');

        return redirect(route('objects.index'));
    }
}
