<?php

namespace App\Http\Controllers;

use App\DataTables\ObjectTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateObjectTypeRequest;
use App\Http\Requests\UpdateObjectTypeRequest;
use App\Repositories\ObjectTypeRepository;
use Doctrine\DBAL\Types\ObjectType;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ObjectTypeController extends AppBaseController
{
    /** @var  ObjectTypeRepository */
    private $objectTypeRepository;

    public function __construct(ObjectTypeRepository $objectTypeRepo)
    {
        $this->objectTypeRepository = $objectTypeRepo;
    }

    /**
     * Display a listing of the ObjectType.
     *
     * @param ObjectTypeDataTable $objectTypeDataTable
     * @return Response
     */
    public function index(ObjectTypeDataTable $objectTypeDataTable)
    {
        return $objectTypeDataTable->render('object_types.index');
    }

    /**
     * Show the form for creating a new ObjectType.
     *
     * @return Response
     */
    public function create()
    {
        return view('object_types.create');
    }

    /**
     * Store a newly created ObjectType in storage.
     *
     * @param CreateObjectTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateObjectTypeRequest $request)
    {
        $input = $request->all();

        $objectType = $this->objectTypeRepository->create($input);

        Flash::success('Object Type saved successfully.');

        return redirect(route('objectTypes.index'));
    }

    /**
     * Display the specified ObjectType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $objectType = $this->objectTypeRepository->findWithoutFail($id);

        if (empty($objectType)) {
            Flash::error('Object Type not found');

            return redirect(route('objectTypes.index'));
        }

        return view('object_types.show')->with('objectType', $objectType);
    }

    /**
     * Show the form for editing the specified ObjectType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $objectType = $this->objectTypeRepository->findWithoutFail($id);

        if (empty($objectType)) {
            Flash::error('Object Type not found');

            return redirect(route('objectTypes.index'));
        }

        return view('object_types.edit')->with('objectType', $objectType);
    }

    /**
     * Update the specified ObjectType in storage.
     *
     * @param  int              $id
     * @param UpdateObjectTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjectTypeRequest $request)
    {
        $objectType = $this->objectTypeRepository->findWithoutFail($id);

        if (empty($objectType)) {
            Flash::error('Object Type not found');

            return redirect(route('objectTypes.index'));
        }

        $objectType = $this->objectTypeRepository->update($request->all(), $id);

        Flash::success('Object Type updated successfully.');

        return redirect(route('objectTypes.index'));
    }

    /**
     * Remove the specified ObjectType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $objectType = $this->objectTypeRepository->findWithoutFail($id);

        if (empty($objectType)) {
            Flash::error('Object Type not found');

            return redirect(route('objectTypes.index'));
        }

        $relatedObjects =\App\Models\ObjectType::has('objectTypeObjects')->where('id',$id)->count();
        if($relatedObjects){
            Flash::error('Сначала удалите объекты, связанные с этим типом');
            return redirect(route('objectTypes.index'));
        }
        else{
            $this->objectTypeRepository->delete($id);
            Flash::success('Object Type deleted successfully.');
            return redirect(route('objectTypes.index'));
        }


    }
}
