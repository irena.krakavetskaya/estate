<?php

namespace App\Http\Controllers;

use App\DataTables\MicroDistrictDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMicroDistrictRequest;
use App\Http\Requests\UpdateMicroDistrictRequest;
use App\Models\MicroDistrict;
use App\Repositories\MicroDistrictRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;

class MicroDistrictController extends AppBaseController
{
    /** @var  MicroDistrictRepository */
    private $microDistrictRepository;

    public function __construct(MicroDistrictRepository $microDistrictRepo)
    {
        $this->microDistrictRepository = $microDistrictRepo;
    }

    /**
     * Display a listing of the MicroDistrict.
     *
     * @param MicroDistrictDataTable $microDistrictDataTable
     * @return Response
     */
    public function index(MicroDistrictDataTable $microDistrictDataTable)
    {
        return $microDistrictDataTable->render('micro_districts.index');
    }

    /**
     * Show the form for creating a new MicroDistrict.
     *
     * @return Response
     */
    public function create()
    {
        //return view('micro_districts.create');
        $districts = $this->microDistrictRepository->getDistricts();
        return view('micro_districts.create',compact('districts'));
    }

    /**
     * Store a newly created MicroDistrict in storage.
     *
     * @param CreateMicroDistrictRequest $request
     *
     * @return Response
     */
    public function store(CreateMicroDistrictRequest $request)
    {
        $input = $request->all();

        $microDistrict = $this->microDistrictRepository->create($input);

        Flash::success('Micro District saved successfully.');

        return redirect(route('microDistricts.index'));
    }

    /**
     * Display the specified MicroDistrict.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $microDistrict = $this->microDistrictRepository->findWithoutFail($id);

        if (empty($microDistrict)) {
            Flash::error('Micro District not found');

            return redirect(route('microDistricts.index'));
        }

        return view('micro_districts.show')->with('microDistrict', $microDistrict);
    }

    /**
     * Show the form for editing the specified MicroDistrict.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $microDistrict = $this->microDistrictRepository->findWithoutFail($id);

        if (empty($microDistrict)) {
            Flash::error('Micro District not found');

            return redirect(route('microDistricts.index'));
        }

        $districts = $this->microDistrictRepository->getDistricts();
        return view('micro_districts.edit',compact('microDistrict','districts'));
        //return view('micro_districts.edit')->with('microDistrict', $microDistrict);
    }

    /**
     * Update the specified MicroDistrict in storage.
     *
     * @param  int              $id
     * @param UpdateMicroDistrictRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMicroDistrictRequest $request)
    {
        $microDistrict = $this->microDistrictRepository->findWithoutFail($id);

        if (empty($microDistrict)) {
            Flash::error('Micro District not found');

            return redirect(route('microDistricts.index'));
        }

        $microDistrict = $this->microDistrictRepository->update($request->all(), $id);

        Flash::success('Micro District updated successfully.');

        return redirect(route('microDistricts.index'));
    }

    /**
     * Remove the specified MicroDistrict from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $microDistrict = $this->microDistrictRepository->findWithoutFail($id);

        if (empty($microDistrict)) {
            Flash::error('Micro District not found');

            return redirect(route('microDistricts.index'));
        }

        $relatedObjects =MicroDistrict::has('microdistrictObjects')->where('id',$id)->count();
        if($relatedObjects){
            Flash::error('Сначала удалите  объекты, связанные с этим микрорайоном');
            return redirect(route('microDistricts.index'));
        }
        else {
            $this->microDistrictRepository->delete($id);
            Flash::success('Micro District deleted successfully.');
            return redirect(route('microDistricts.index'));
        }
    }

    public function upload(Request $request)
    {
        $file = $request->file('microDistricts');

        $microdistricts = $this->microDistrictRepository->getModel();//$city=new City();
        $arr=$microdistricts->fillable;

        $required=['name','micro_district_id'];
        $foreign_key=['district_id'];

        return $this->uploadFile($file, 'microDistricts', $microdistricts, $foreign_key, $arr, $required);
    }

    public function download()
    {
        $microdistricts=MicroDistrict::get();

        return $this->downloadFile($microdistricts,  'Micro_district');
    }
}
