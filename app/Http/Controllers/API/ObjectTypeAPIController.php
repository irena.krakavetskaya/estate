<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateObjectTypeAPIRequest;
use App\Http\Requests\API\UpdateObjectTypeAPIRequest;
use App\Models\ObjectType;
use App\Repositories\ObjectTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ObjectTypeController
 * @package App\Http\Controllers\API
 */

class ObjectTypeAPIController extends AppBaseController
{
    /** @var  ObjectTypeRepository */
    private $objectTypeRepository;

    public function __construct(ObjectTypeRepository $objectTypeRepo)
    {
        $this->objectTypeRepository = $objectTypeRepo;
    }

    /**
     * Display a listing of the ObjectType.
     * GET|HEAD /objectTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->objectTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->objectTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $objectTypes = $this->objectTypeRepository->all();

        return $this->sendResponse($objectTypes->toArray(), 'Object Types retrieved successfully');
    }

    /**
     * Store a newly created ObjectType in storage.
     * POST /objectTypes
     *
     * @param CreateObjectTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateObjectTypeAPIRequest $request)
    {
        $input = $request->all();

        $objectTypes = $this->objectTypeRepository->create($input);

        return $this->sendResponse($objectTypes->toArray(), 'Object Type saved successfully');
    }

    /**
     * Display the specified ObjectType.
     * GET|HEAD /objectTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ObjectType $objectType */
        $objectType = $this->objectTypeRepository->findWithoutFail($id);

        if (empty($objectType)) {
            return $this->sendError('Object Type not found');
        }

        return $this->sendResponse($objectType->toArray(), 'Object Type retrieved successfully');
    }

    /**
     * Update the specified ObjectType in storage.
     * PUT/PATCH /objectTypes/{id}
     *
     * @param  int $id
     * @param UpdateObjectTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjectTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ObjectType $objectType */
        $objectType = $this->objectTypeRepository->findWithoutFail($id);

        if (empty($objectType)) {
            return $this->sendError('Object Type not found');
        }

        $objectType = $this->objectTypeRepository->update($input, $id);

        return $this->sendResponse($objectType->toArray(), 'ObjectType updated successfully');
    }

    /**
     * Remove the specified ObjectType from storage.
     * DELETE /objectTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ObjectType $objectType */
        $objectType = $this->objectTypeRepository->findWithoutFail($id);

        if (empty($objectType)) {
            return $this->sendError('Object Type not found');
        }

        $objectType->delete();

        return $this->sendResponse($id, 'Object Type deleted successfully');
    }
}
