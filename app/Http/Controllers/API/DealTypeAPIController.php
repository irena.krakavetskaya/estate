<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealTypeAPIRequest;
use App\Http\Requests\API\UpdateDealTypeAPIRequest;
use App\Models\DealType;
use App\Repositories\DealTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DealTypeController
 * @package App\Http\Controllers\API
 */

class DealTypeAPIController extends AppBaseController
{
    /** @var  DealTypeRepository */
    private $dealTypeRepository;

    public function __construct(DealTypeRepository $dealTypeRepo)
    {
        $this->dealTypeRepository = $dealTypeRepo;
    }

    /**
     * Display a listing of the DealType.
     * GET|HEAD /dealTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->dealTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->dealTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $dealTypes = $this->dealTypeRepository->all();

        return $this->sendResponse($dealTypes->toArray(), 'Deal Types retrieved successfully');
    }

    /**
     * Store a newly created DealType in storage.
     * POST /dealTypes
     *
     * @param CreateDealTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDealTypeAPIRequest $request)
    {
        $input = $request->all();

        $dealTypes = $this->dealTypeRepository->create($input);

        return $this->sendResponse($dealTypes->toArray(), 'Deal Type saved successfully');
    }

    /**
     * Display the specified DealType.
     * GET|HEAD /dealTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DealType $dealType */
        $dealType = $this->dealTypeRepository->findWithoutFail($id);

        if (empty($dealType)) {
            return $this->sendError('Deal Type not found');
        }

        return $this->sendResponse($dealType->toArray(), 'Deal Type retrieved successfully');
    }

    /**
     * Update the specified DealType in storage.
     * PUT/PATCH /dealTypes/{id}
     *
     * @param  int $id
     * @param UpdateDealTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDealTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var DealType $dealType */
        $dealType = $this->dealTypeRepository->findWithoutFail($id);

        if (empty($dealType)) {
            return $this->sendError('Deal Type not found');
        }

        $dealType = $this->dealTypeRepository->update($input, $id);

        return $this->sendResponse($dealType->toArray(), 'DealType updated successfully');
    }

    /**
     * Remove the specified DealType from storage.
     * DELETE /dealTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DealType $dealType */
        $dealType = $this->dealTypeRepository->findWithoutFail($id);

        if (empty($dealType)) {
            return $this->sendError('Deal Type not found');
        }

        $dealType->delete();

        return $this->sendResponse($id, 'Deal Type deleted successfully');
    }
}
