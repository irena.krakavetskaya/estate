<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMicroDistrictAPIRequest;
use App\Http\Requests\API\UpdateMicroDistrictAPIRequest;
use App\Models\MicroDistrict;
use App\Repositories\MicroDistrictRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MicroDistrictController
 * @package App\Http\Controllers\API
 */

class MicroDistrictAPIController extends AppBaseController
{
    /** @var  MicroDistrictRepository */
    private $microDistrictRepository;

    public function __construct(MicroDistrictRepository $microDistrictRepo)
    {
        $this->microDistrictRepository = $microDistrictRepo;
    }

    /**
     * Display a listing of the MicroDistrict.
     * GET|HEAD /microDistricts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->microDistrictRepository->pushCriteria(new RequestCriteria($request));
        $this->microDistrictRepository->pushCriteria(new LimitOffsetCriteria($request));
        $microDistricts = $this->microDistrictRepository->all();

        return $this->sendResponse($microDistricts->toArray(), 'Micro Districts retrieved successfully');
    }

    /**
     * Store a newly created MicroDistrict in storage.
     * POST /microDistricts
     *
     * @param CreateMicroDistrictAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMicroDistrictAPIRequest $request)
    {
        $input = $request->all();

        $microDistricts = $this->microDistrictRepository->create($input);

        return $this->sendResponse($microDistricts->toArray(), 'Micro District saved successfully');
    }

    /**
     * Display the specified MicroDistrict.
     * GET|HEAD /microDistricts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MicroDistrict $microDistrict */
        $microDistrict = $this->microDistrictRepository->findWithoutFail($id);

        if (empty($microDistrict)) {
            return $this->sendError('Micro District not found');
        }

        return $this->sendResponse($microDistrict->toArray(), 'Micro District retrieved successfully');
    }

    /**
     * Update the specified MicroDistrict in storage.
     * PUT/PATCH /microDistricts/{id}
     *
     * @param  int $id
     * @param UpdateMicroDistrictAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMicroDistrictAPIRequest $request)
    {
        $input = $request->all();

        /** @var MicroDistrict $microDistrict */
        $microDistrict = $this->microDistrictRepository->findWithoutFail($id);

        if (empty($microDistrict)) {
            return $this->sendError('Micro District not found');
        }

        $microDistrict = $this->microDistrictRepository->update($input, $id);

        return $this->sendResponse($microDistrict->toArray(), 'MicroDistrict updated successfully');
    }

    /**
     * Remove the specified MicroDistrict from storage.
     * DELETE /microDistricts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MicroDistrict $microDistrict */
        $microDistrict = $this->microDistrictRepository->findWithoutFail($id);

        if (empty($microDistrict)) {
            return $this->sendError('Micro District not found');
        }

        $microDistrict->delete();

        return $this->sendResponse($id, 'Micro District deleted successfully');
    }
}
