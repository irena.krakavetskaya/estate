<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateObjectAPIRequest;
use App\Http\Requests\API\UpdateObjectAPIRequest;
use App\Models\Object;
use App\Repositories\ObjectRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ObjectController
 * @package App\Http\Controllers\API
 */

class ObjectAPIController extends AppBaseController
{
    /** @var  ObjectRepository */
    private $objectRepository;

    public function __construct(ObjectRepository $objectRepo)
    {
        $this->objectRepository = $objectRepo;
    }

    /**
     * Display a listing of the Object.
     * GET|HEAD /objects
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->objectRepository->pushCriteria(new RequestCriteria($request));
        $this->objectRepository->pushCriteria(new LimitOffsetCriteria($request));
        $objects = $this->objectRepository->all();

        return $this->sendResponse($objects->toArray(), 'Objects retrieved successfully');
    }

    /**
     * Store a newly created Object in storage.
     * POST /objects
     *
     * @param CreateObjectAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateObjectAPIRequest $request)
    {
        $input = $request->all();

        $objects = $this->objectRepository->create($input);

        return $this->sendResponse($objects->toArray(), 'Object saved successfully');
    }

    /**
     * Display the specified Object.
     * GET|HEAD /objects/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Object $object */
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            return $this->sendError('Object not found');
        }

        return $this->sendResponse($object->toArray(), 'Object retrieved successfully');
    }

    /**
     * Update the specified Object in storage.
     * PUT/PATCH /objects/{id}
     *
     * @param  int $id
     * @param UpdateObjectAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjectAPIRequest $request)
    {
        $input = $request->all();

        /** @var Object $object */
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            return $this->sendError('Object not found');
        }

        $object = $this->objectRepository->update($input, $id);

        return $this->sendResponse($object->toArray(), 'Object updated successfully');
    }

    /**
     * Remove the specified Object from storage.
     * DELETE /objects/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Object $object */
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            return $this->sendError('Object not found');
        }

        $object->delete();

        return $this->sendResponse($id, 'Object deleted successfully');
    }
}
