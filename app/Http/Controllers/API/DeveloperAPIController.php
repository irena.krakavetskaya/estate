<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeveloperAPIRequest;
use App\Http\Requests\API\UpdateDeveloperAPIRequest;
use App\Models\Developer;
use App\Repositories\DeveloperRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DeveloperController
 * @package App\Http\Controllers\API
 */

class DeveloperAPIController extends AppBaseController
{
    /** @var  DeveloperRepository */
    private $developerRepository;

    public function __construct(DeveloperRepository $developerRepo)
    {
        $this->developerRepository = $developerRepo;
    }

    /**
     * Display a listing of the Developer.
     * GET|HEAD /developers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->developerRepository->pushCriteria(new RequestCriteria($request));
        $this->developerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $developers = $this->developerRepository->all();

        return $this->sendResponse($developers->toArray(), 'Developers retrieved successfully');
    }

    /**
     * Store a newly created Developer in storage.
     * POST /developers
     *
     * @param CreateDeveloperAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeveloperAPIRequest $request)
    {
        $input = $request->all();

        $developers = $this->developerRepository->create($input);

        return $this->sendResponse($developers->toArray(), 'Developer saved successfully');
    }

    /**
     * Display the specified Developer.
     * GET|HEAD /developers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Developer $developer */
        $developer = $this->developerRepository->findWithoutFail($id);

        if (empty($developer)) {
            return $this->sendError('Developer not found');
        }

        return $this->sendResponse($developer->toArray(), 'Developer retrieved successfully');
    }

    /**
     * Update the specified Developer in storage.
     * PUT/PATCH /developers/{id}
     *
     * @param  int $id
     * @param UpdateDeveloperAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeveloperAPIRequest $request)
    {
        $input = $request->all();

        /** @var Developer $developer */
        $developer = $this->developerRepository->findWithoutFail($id);

        if (empty($developer)) {
            return $this->sendError('Developer not found');
        }

        $developer = $this->developerRepository->update($input, $id);

        return $this->sendResponse($developer->toArray(), 'Developer updated successfully');
    }

    /**
     * Remove the specified Developer from storage.
     * DELETE /developers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Developer $developer */
        $developer = $this->developerRepository->findWithoutFail($id);

        if (empty($developer)) {
            return $this->sendError('Developer not found');
        }

        $developer->delete();

        return $this->sendResponse($id, 'Developer deleted successfully');
    }
}
