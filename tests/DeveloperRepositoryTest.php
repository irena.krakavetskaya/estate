<?php

use App\Models\Developer;
use App\Repositories\DeveloperRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeveloperRepositoryTest extends TestCase
{
    use MakeDeveloperTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeveloperRepository
     */
    protected $developerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->developerRepo = App::make(DeveloperRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateDeveloper()
    {
        $developer = $this->fakeDeveloperData();
        $createdDeveloper = $this->developerRepo->create($developer);
        $createdDeveloper = $createdDeveloper->toArray();
        $this->assertArrayHasKey('id', $createdDeveloper);
        $this->assertNotNull($createdDeveloper['id'], 'Created Developer must have id specified');
        $this->assertNotNull(Developer::find($createdDeveloper['id']), 'Developer with given id must be in DB');
        $this->assertModelData($developer, $createdDeveloper);
    }

    /**
     * @test read
     */
    public function testReadDeveloper()
    {
        $developer = $this->makeDeveloper();
        $dbDeveloper = $this->developerRepo->find($developer->id);
        $dbDeveloper = $dbDeveloper->toArray();
        $this->assertModelData($developer->toArray(), $dbDeveloper);
    }

    /**
     * @test update
     */
    public function testUpdateDeveloper()
    {
        $developer = $this->makeDeveloper();
        $fakeDeveloper = $this->fakeDeveloperData();
        $updatedDeveloper = $this->developerRepo->update($fakeDeveloper, $developer->id);
        $this->assertModelData($fakeDeveloper, $updatedDeveloper->toArray());
        $dbDeveloper = $this->developerRepo->find($developer->id);
        $this->assertModelData($fakeDeveloper, $dbDeveloper->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteDeveloper()
    {
        $developer = $this->makeDeveloper();
        $resp = $this->developerRepo->delete($developer->id);
        $this->assertTrue($resp);
        $this->assertNull(Developer::find($developer->id), 'Developer should not exist in DB');
    }
}
