<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DealTypeApiTest extends TestCase
{
    use MakeDealTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateDealType()
    {
        $dealType = $this->fakeDealTypeData();
        $this->json('POST', '/api/v1/dealTypes', $dealType);

        $this->assertApiResponse($dealType);
    }

    /**
     * @test
     */
    public function testReadDealType()
    {
        $dealType = $this->makeDealType();
        $this->json('GET', '/api/v1/dealTypes/'.$dealType->id);

        $this->assertApiResponse($dealType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateDealType()
    {
        $dealType = $this->makeDealType();
        $editedDealType = $this->fakeDealTypeData();

        $this->json('PUT', '/api/v1/dealTypes/'.$dealType->id, $editedDealType);

        $this->assertApiResponse($editedDealType);
    }

    /**
     * @test
     */
    public function testDeleteDealType()
    {
        $dealType = $this->makeDealType();
        $this->json('DELETE', '/api/v1/dealTypes/'.$dealType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/dealTypes/'.$dealType->id);

        $this->assertResponseStatus(404);
    }
}
