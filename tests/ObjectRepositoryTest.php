<?php

use App\Models\Object;
use App\Repositories\ObjectRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ObjectRepositoryTest extends TestCase
{
    use MakeObjectTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ObjectRepository
     */
    protected $objectRepo;

    public function setUp()
    {
        parent::setUp();
        $this->objectRepo = App::make(ObjectRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateObject()
    {
        $object = $this->fakeObjectData();
        $createdObject = $this->objectRepo->create($object);
        $createdObject = $createdObject->toArray();
        $this->assertArrayHasKey('id', $createdObject);
        $this->assertNotNull($createdObject['id'], 'Created Object must have id specified');
        $this->assertNotNull(Object::find($createdObject['id']), 'Object with given id must be in DB');
        $this->assertModelData($object, $createdObject);
    }

    /**
     * @test read
     */
    public function testReadObject()
    {
        $object = $this->makeObject();
        $dbObject = $this->objectRepo->find($object->id);
        $dbObject = $dbObject->toArray();
        $this->assertModelData($object->toArray(), $dbObject);
    }

    /**
     * @test update
     */
    public function testUpdateObject()
    {
        $object = $this->makeObject();
        $fakeObject = $this->fakeObjectData();
        $updatedObject = $this->objectRepo->update($fakeObject, $object->id);
        $this->assertModelData($fakeObject, $updatedObject->toArray());
        $dbObject = $this->objectRepo->find($object->id);
        $this->assertModelData($fakeObject, $dbObject->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteObject()
    {
        $object = $this->makeObject();
        $resp = $this->objectRepo->delete($object->id);
        $this->assertTrue($resp);
        $this->assertNull(Object::find($object->id), 'Object should not exist in DB');
    }
}
