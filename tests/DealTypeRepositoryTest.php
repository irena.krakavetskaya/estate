<?php

use App\Models\DealType;
use App\Repositories\DealTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DealTypeRepositoryTest extends TestCase
{
    use MakeDealTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var DealTypeRepository
     */
    protected $dealTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->dealTypeRepo = App::make(DealTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateDealType()
    {
        $dealType = $this->fakeDealTypeData();
        $createdDealType = $this->dealTypeRepo->create($dealType);
        $createdDealType = $createdDealType->toArray();
        $this->assertArrayHasKey('id', $createdDealType);
        $this->assertNotNull($createdDealType['id'], 'Created DealType must have id specified');
        $this->assertNotNull(DealType::find($createdDealType['id']), 'DealType with given id must be in DB');
        $this->assertModelData($dealType, $createdDealType);
    }

    /**
     * @test read
     */
    public function testReadDealType()
    {
        $dealType = $this->makeDealType();
        $dbDealType = $this->dealTypeRepo->find($dealType->id);
        $dbDealType = $dbDealType->toArray();
        $this->assertModelData($dealType->toArray(), $dbDealType);
    }

    /**
     * @test update
     */
    public function testUpdateDealType()
    {
        $dealType = $this->makeDealType();
        $fakeDealType = $this->fakeDealTypeData();
        $updatedDealType = $this->dealTypeRepo->update($fakeDealType, $dealType->id);
        $this->assertModelData($fakeDealType, $updatedDealType->toArray());
        $dbDealType = $this->dealTypeRepo->find($dealType->id);
        $this->assertModelData($fakeDealType, $dbDealType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteDealType()
    {
        $dealType = $this->makeDealType();
        $resp = $this->dealTypeRepo->delete($dealType->id);
        $this->assertTrue($resp);
        $this->assertNull(DealType::find($dealType->id), 'DealType should not exist in DB');
    }
}
