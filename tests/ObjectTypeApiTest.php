<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ObjectTypeApiTest extends TestCase
{
    use MakeObjectTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateObjectType()
    {
        $objectType = $this->fakeObjectTypeData();
        $this->json('POST', '/api/v1/objectTypes', $objectType);

        $this->assertApiResponse($objectType);
    }

    /**
     * @test
     */
    public function testReadObjectType()
    {
        $objectType = $this->makeObjectType();
        $this->json('GET', '/api/v1/objectTypes/'.$objectType->id);

        $this->assertApiResponse($objectType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateObjectType()
    {
        $objectType = $this->makeObjectType();
        $editedObjectType = $this->fakeObjectTypeData();

        $this->json('PUT', '/api/v1/objectTypes/'.$objectType->id, $editedObjectType);

        $this->assertApiResponse($editedObjectType);
    }

    /**
     * @test
     */
    public function testDeleteObjectType()
    {
        $objectType = $this->makeObjectType();
        $this->json('DELETE', '/api/v1/objectTypes/'.$objectType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/objectTypes/'.$objectType->id);

        $this->assertResponseStatus(404);
    }
}
