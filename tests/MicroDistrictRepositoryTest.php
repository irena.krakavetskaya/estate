<?php

use App\Models\MicroDistrict;
use App\Repositories\MicroDistrictRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MicroDistrictRepositoryTest extends TestCase
{
    use MakeMicroDistrictTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MicroDistrictRepository
     */
    protected $microDistrictRepo;

    public function setUp()
    {
        parent::setUp();
        $this->microDistrictRepo = App::make(MicroDistrictRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMicroDistrict()
    {
        $microDistrict = $this->fakeMicroDistrictData();
        $createdMicroDistrict = $this->microDistrictRepo->create($microDistrict);
        $createdMicroDistrict = $createdMicroDistrict->toArray();
        $this->assertArrayHasKey('id', $createdMicroDistrict);
        $this->assertNotNull($createdMicroDistrict['id'], 'Created MicroDistrict must have id specified');
        $this->assertNotNull(MicroDistrict::find($createdMicroDistrict['id']), 'MicroDistrict with given id must be in DB');
        $this->assertModelData($microDistrict, $createdMicroDistrict);
    }

    /**
     * @test read
     */
    public function testReadMicroDistrict()
    {
        $microDistrict = $this->makeMicroDistrict();
        $dbMicroDistrict = $this->microDistrictRepo->find($microDistrict->id);
        $dbMicroDistrict = $dbMicroDistrict->toArray();
        $this->assertModelData($microDistrict->toArray(), $dbMicroDistrict);
    }

    /**
     * @test update
     */
    public function testUpdateMicroDistrict()
    {
        $microDistrict = $this->makeMicroDistrict();
        $fakeMicroDistrict = $this->fakeMicroDistrictData();
        $updatedMicroDistrict = $this->microDistrictRepo->update($fakeMicroDistrict, $microDistrict->id);
        $this->assertModelData($fakeMicroDistrict, $updatedMicroDistrict->toArray());
        $dbMicroDistrict = $this->microDistrictRepo->find($microDistrict->id);
        $this->assertModelData($fakeMicroDistrict, $dbMicroDistrict->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMicroDistrict()
    {
        $microDistrict = $this->makeMicroDistrict();
        $resp = $this->microDistrictRepo->delete($microDistrict->id);
        $this->assertTrue($resp);
        $this->assertNull(MicroDistrict::find($microDistrict->id), 'MicroDistrict should not exist in DB');
    }
}
