<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MicroDistrictApiTest extends TestCase
{
    use MakeMicroDistrictTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMicroDistrict()
    {
        $microDistrict = $this->fakeMicroDistrictData();
        $this->json('POST', '/api/v1/microDistricts', $microDistrict);

        $this->assertApiResponse($microDistrict);
    }

    /**
     * @test
     */
    public function testReadMicroDistrict()
    {
        $microDistrict = $this->makeMicroDistrict();
        $this->json('GET', '/api/v1/microDistricts/'.$microDistrict->id);

        $this->assertApiResponse($microDistrict->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMicroDistrict()
    {
        $microDistrict = $this->makeMicroDistrict();
        $editedMicroDistrict = $this->fakeMicroDistrictData();

        $this->json('PUT', '/api/v1/microDistricts/'.$microDistrict->id, $editedMicroDistrict);

        $this->assertApiResponse($editedMicroDistrict);
    }

    /**
     * @test
     */
    public function testDeleteMicroDistrict()
    {
        $microDistrict = $this->makeMicroDistrict();
        $this->json('DELETE', '/api/v1/microDistricts/'.$microDistrict->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/microDistricts/'.$microDistrict->id);

        $this->assertResponseStatus(404);
    }
}
