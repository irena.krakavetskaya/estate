<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeveloperApiTest extends TestCase
{
    use MakeDeveloperTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateDeveloper()
    {
        $developer = $this->fakeDeveloperData();
        $this->json('POST', '/api/v1/developers', $developer);

        $this->assertApiResponse($developer);
    }

    /**
     * @test
     */
    public function testReadDeveloper()
    {
        $developer = $this->makeDeveloper();
        $this->json('GET', '/api/v1/developers/'.$developer->id);

        $this->assertApiResponse($developer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateDeveloper()
    {
        $developer = $this->makeDeveloper();
        $editedDeveloper = $this->fakeDeveloperData();

        $this->json('PUT', '/api/v1/developers/'.$developer->id, $editedDeveloper);

        $this->assertApiResponse($editedDeveloper);
    }

    /**
     * @test
     */
    public function testDeleteDeveloper()
    {
        $developer = $this->makeDeveloper();
        $this->json('DELETE', '/api/v1/developers/'.$developer->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/developers/'.$developer->id);

        $this->assertResponseStatus(404);
    }
}
