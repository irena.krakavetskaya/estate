<?php

use Faker\Factory as Faker;
use App\Models\ObjectType;
use App\Repositories\ObjectTypeRepository;

trait MakeObjectTypeTrait
{
    /**
     * Create fake instance of ObjectType and save it in database
     *
     * @param array $objectTypeFields
     * @return ObjectType
     */
    public function makeObjectType($objectTypeFields = [])
    {
        /** @var ObjectTypeRepository $objectTypeRepo */
        $objectTypeRepo = App::make(ObjectTypeRepository::class);
        $theme = $this->fakeObjectTypeData($objectTypeFields);
        return $objectTypeRepo->create($theme);
    }

    /**
     * Get fake instance of ObjectType
     *
     * @param array $objectTypeFields
     * @return ObjectType
     */
    public function fakeObjectType($objectTypeFields = [])
    {
        return new ObjectType($this->fakeObjectTypeData($objectTypeFields));
    }

    /**
     * Get fake data of ObjectType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeObjectTypeData($objectTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'object_type' => $fake->word,
            'name' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $objectTypeFields);
    }
}
