<?php

use Faker\Factory as Faker;
use App\Models\DealType;
use App\Repositories\DealTypeRepository;

trait MakeDealTypeTrait
{
    /**
     * Create fake instance of DealType and save it in database
     *
     * @param array $dealTypeFields
     * @return DealType
     */
    public function makeDealType($dealTypeFields = [])
    {
        /** @var DealTypeRepository $dealTypeRepo */
        $dealTypeRepo = App::make(DealTypeRepository::class);
        $theme = $this->fakeDealTypeData($dealTypeFields);
        return $dealTypeRepo->create($theme);
    }

    /**
     * Get fake instance of DealType
     *
     * @param array $dealTypeFields
     * @return DealType
     */
    public function fakeDealType($dealTypeFields = [])
    {
        return new DealType($this->fakeDealTypeData($dealTypeFields));
    }

    /**
     * Get fake data of DealType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDealTypeData($dealTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'deal_type' => $fake->word,
            'name' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $dealTypeFields);
    }
}
