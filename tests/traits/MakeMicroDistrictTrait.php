<?php

use Faker\Factory as Faker;
use App\Models\MicroDistrict;
use App\Repositories\MicroDistrictRepository;

trait MakeMicroDistrictTrait
{
    /**
     * Create fake instance of MicroDistrict and save it in database
     *
     * @param array $microDistrictFields
     * @return MicroDistrict
     */
    public function makeMicroDistrict($microDistrictFields = [])
    {
        /** @var MicroDistrictRepository $microDistrictRepo */
        $microDistrictRepo = App::make(MicroDistrictRepository::class);
        $theme = $this->fakeMicroDistrictData($microDistrictFields);
        return $microDistrictRepo->create($theme);
    }

    /**
     * Get fake instance of MicroDistrict
     *
     * @param array $microDistrictFields
     * @return MicroDistrict
     */
    public function fakeMicroDistrict($microDistrictFields = [])
    {
        return new MicroDistrict($this->fakeMicroDistrictData($microDistrictFields));
    }

    /**
     * Get fake data of MicroDistrict
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMicroDistrictData($microDistrictFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'district_id' => $fake->randomDigitNotNull,
            'micro_district_id' => $fake->randomDigitNotNull,
            'name' => $fake->text,
            'translit' => $fake->word,
            'roditelniy' => $fake->text,
            'predlozhniy' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $microDistrictFields);
    }
}
