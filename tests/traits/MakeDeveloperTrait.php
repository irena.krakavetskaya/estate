<?php

use Faker\Factory as Faker;
use App\Models\Developer;
use App\Repositories\DeveloperRepository;

trait MakeDeveloperTrait
{
    /**
     * Create fake instance of Developer and save it in database
     *
     * @param array $developerFields
     * @return Developer
     */
    public function makeDeveloper($developerFields = [])
    {
        /** @var DeveloperRepository $developerRepo */
        $developerRepo = App::make(DeveloperRepository::class);
        $theme = $this->fakeDeveloperData($developerFields);
        return $developerRepo->create($theme);
    }

    /**
     * Get fake instance of Developer
     *
     * @param array $developerFields
     * @return Developer
     */
    public function fakeDeveloper($developerFields = [])
    {
        return new Developer($this->fakeDeveloperData($developerFields));
    }

    /**
     * Get fake data of Developer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDeveloperData($developerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'dev_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'legal' => $fake->text,
            'html' => $fake->text,
            'web' => $fake->text,
            'email' => $fake->text,
            'address' => $fake->text,
            'logo' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $developerFields);
    }
}
