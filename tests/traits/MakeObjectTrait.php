<?php

use Faker\Factory as Faker;
use App\Models\Object;
use App\Repositories\ObjectRepository;

trait MakeObjectTrait
{
    /**
     * Create fake instance of Object and save it in database
     *
     * @param array $objectFields
     * @return Object
     */
    public function makeObject($objectFields = [])
    {
        /** @var ObjectRepository $objectRepo */
        $objectRepo = App::make(ObjectRepository::class);
        $theme = $this->fakeObjectData($objectFields);
        return $objectRepo->create($theme);
    }

    /**
     * Get fake instance of Object
     *
     * @param array $objectFields
     * @return Object
     */
    public function fakeObject($objectFields = [])
    {
        return new Object($this->fakeObjectData($objectFields));
    }

    /**
     * Get fake data of Object
     *
     * @param array $postFields
     * @return array
     */
    public function fakeObjectData($objectFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'micro_district_id' => $fake->randomDigitNotNull,
            'deal_type_id' => $fake->randomDigitNotNull,
            'object_type_id' => $fake->randomDigitNotNull,
            'name' => $fake->text,
            'number_of_storeys' => $fake->randomDigitNotNull,
            'type' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $objectFields);
    }
}
