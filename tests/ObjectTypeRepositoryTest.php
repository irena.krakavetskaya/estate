<?php

use App\Models\ObjectType;
use App\Repositories\ObjectTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ObjectTypeRepositoryTest extends TestCase
{
    use MakeObjectTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ObjectTypeRepository
     */
    protected $objectTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->objectTypeRepo = App::make(ObjectTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateObjectType()
    {
        $objectType = $this->fakeObjectTypeData();
        $createdObjectType = $this->objectTypeRepo->create($objectType);
        $createdObjectType = $createdObjectType->toArray();
        $this->assertArrayHasKey('id', $createdObjectType);
        $this->assertNotNull($createdObjectType['id'], 'Created ObjectType must have id specified');
        $this->assertNotNull(ObjectType::find($createdObjectType['id']), 'ObjectType with given id must be in DB');
        $this->assertModelData($objectType, $createdObjectType);
    }

    /**
     * @test read
     */
    public function testReadObjectType()
    {
        $objectType = $this->makeObjectType();
        $dbObjectType = $this->objectTypeRepo->find($objectType->id);
        $dbObjectType = $dbObjectType->toArray();
        $this->assertModelData($objectType->toArray(), $dbObjectType);
    }

    /**
     * @test update
     */
    public function testUpdateObjectType()
    {
        $objectType = $this->makeObjectType();
        $fakeObjectType = $this->fakeObjectTypeData();
        $updatedObjectType = $this->objectTypeRepo->update($fakeObjectType, $objectType->id);
        $this->assertModelData($fakeObjectType, $updatedObjectType->toArray());
        $dbObjectType = $this->objectTypeRepo->find($objectType->id);
        $this->assertModelData($fakeObjectType, $dbObjectType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteObjectType()
    {
        $objectType = $this->makeObjectType();
        $resp = $this->objectTypeRepo->delete($objectType->id);
        $this->assertTrue($resp);
        $this->assertNull(ObjectType::find($objectType->id), 'ObjectType should not exist in DB');
    }
}
