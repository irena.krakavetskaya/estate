<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_id', 'City Id:') !!}
    {!! Form::number('city_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('region', 'Region:') !!}
    {!! Form::text('region', null, ['class' => 'form-control']) !!}
</div>

<!-- Translit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('translit', 'Translit:') !!}
    {!! Form::text('translit', null, ['class' => 'form-control']) !!}
</div>

<!-- Roditelniy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('roditelniy', 'Roditelniy:') !!}
    {!! Form::text('roditelniy', null, ['class' => 'form-control']) !!}
</div>

<!-- Predlozhniy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('predlozhniy', 'Predlozhniy:') !!}
    {!! Form::text('predlozhniy', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cities.index') !!}" class="btn btn-default">Отмена</a>
</div>
