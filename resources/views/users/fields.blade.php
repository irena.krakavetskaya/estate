<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

@if (!isset($user->password))
    <div class="form-group col-sm-6">
        {!! Form::label('password', 'Пароль:') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('password', 'Подтверждение пароля:') !!}
        <input type="password" name="password_confirmation" class="form-control">
    </div>
@endif

<div class="form-group col-sm-6">
    {!! Form::label('roles', 'Роль:') !!}
    @isset($role)
        @if($role!=[])
            {!!Form::select('roles', $roles, $role[0], ['class' => 'form-control'])!!}
        @else
            {!!Form::select('roles', $roles, null, ['class' => 'form-control'])!!}
        @endif
    @else
        {!!Form::select('roles', $roles, null, ['class' => 'form-control'])!!}
    @endisset
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Отмена</a>
</div>
