<style>
    .departments hr{
        border-top: 1px solid red;
    }
    .section .form-control#color {
        width: initial;
        display: inline-block;
        float: left;
    }
    .section .color{
        height: 34px;
        padding: 6px 12px;
        background: #fff;
    }
    .filter-wrapper{
        /*padding: 10px 0 20px;*/
        border-bottom: 1px solid #000;
       /* margin-bottom: 20px;*/
    }
    .check-area{
        line-height: 59px;
    }
    .check-area label{
        margin-bottom:0;
        padding-top: 10px;
    }
    .filter .btn{
        margin-top: 20px;
    }
    .toggle-filter{
        margin-bottom: 10px;
    }
    .filter-wrapper .form-control{
        margin-bottom: 20px;
        width:100%;
    }
    .filter-wrapper select{
       max-width: 100%;
    }
    .filter-wrapper .form-group {
        /*text-align: center;*/
    }
    .changeVisibility,.dataTables_filter{
        margin-top: 25px;
    }

    .status .active{ /*option[selected='selected']*/
        background:green;
    }
    .status .inactive{
        background:red;
    }
    .status select{
        color:#fff;
    }
    .status option{
        color:#000;
        background:#fff;
    }
    .deposit .row{
        margin-right: -10px;
        margin-left: -10px;
    }
    .deposit .section {
        border-bottom: 3px solid #403136;
        padding-bottom: 25px;
        padding-top: 10px;
    }
    .deposit .section:nth-last-of-type(2) {
        border: none;
    }
    .deposit .checkbox{
        margin-bottom: 0px;
    }
    .divider{
        border-top: 1px solid #111;
    }
    .divider-line{
        color: rgb(51, 51, 51);
    }
    .investment_rates .dash{
        margin-left: 30px;
    }
    .investment_rates .sum_from input{
        width: 80%;
        display: inline-block;
    }
    .investment_rates  .form-group {
        margin-top: 20px;
    }



    table.dataTable.fixedHeader-floating {
        margin-top: 0px !important;
    }
    table.dataTable tbody tr.grey-inactive {
        background-color: #ecf0f5;
    }
    tr.grey-inactive td{
        opacity: 0.5;
    }
    .currency  textarea.form-control{
        height: 145px;
    }




    .content.deposit td,.content.deposit th{
        text-align: left;
    }
    .text{
        text-align: left;
    }
    .organizations #uploadedimage {
        max-width: 20%;
    }
    .gallery>div>div:last-of-type {
        line-height: 175px;
        text-align: center;
    }
    .gallery>div>div {
        text-align: center;
    }
    #uploadedimage{
        vertical-align: top;
    }
    #img_dep{
        margin: 0 0 10px;
        line-height: 1px;
    }
    .gallery img {
        height: 100px;
        width: auto;
        max-width: 120px;
        margin: 0 auto;
    }
    .insurance, .cancellation, .capitalization, .withdraw{
        text-align: center;
    }
    .hint{
        font-style: italic;
        color: #949292;
        margin-top: 100px;
    }
    .departments .dataTable img{
        max-width:50px;
        padding-right: 2px;
    }
    .departments .dataTable a{
        display: inline-block;
    }
    .sidebar-menu>li>a {
        padding: 12px 5px 12px 10px;
    }
    tr>.general:nth-of-type(8){
        word-break: break-all;
    }
    .deposit div>h4{
        margin-bottom: 0;
    }
    .deposit img{
        padding: 5px 0 0px 0px;
        /*padding: 5px 0 30px 20px;*/
    }
    .text{
        height: 120px;
        overflow-y: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 6;
        -webkit-box-orient: vertical;
    }
    .days>div>div{
        text-align: center;
        line-height: 34px;
    }
    .days label{
        display: block;
    }
    textarea.form-control {
        height: 130px;
    }
    .deposit .tab-content {
        border: none;
    }
    .deposit #dataTableBuilder_filter{
        margin-top: 15px;
    }
    .deposit textarea{
        height: 100px;
    }
    textarea[name="describing"]{
        height: 100px; /*221px;*/
    }
    .dep-divider{
        margin-bottom: 50px;
    }
    textarea[name='about_bank']{
        height: 245px;
    }
    input[name='activity'], input[name='products[]'], div>input[type='checkbox']{
        margin-top: 0;
        vertical-align: middle;
        margin-bottom: 2px
    }
    input[name='products[]']+span, input[name='countries[]']+span{
        margin-right: 10px;
    }
    .event .dataTable a>img {
        max-width: 80px;
    }

    .schedule-row{
        line-height: 34px;
        padding-bottom: 10px;
        height: 44px;
    }
    .schedule-block{
        margin-bottom: 20px;
    }
    .schedule-row>div:not(:first-of-type){
        text-align: right;
    }
    .schedule-row>div:last-of-type{
        text-align: left;
    }
    .schedule-block  .form-group{
        margin-top: 15px;
    }
    .week h1 strong{
        font-size: 18px;
        font-weight: 500;
    }

    .gallery.timetable img {
        max-height: 400px;
        height: auto !important;
    }
    .gallery.timetable>div>div:nth-child(4n) {
        /*clear: left;*/
    }
    .gallery.timetable>div>div:last-of-type {
        line-height: 400px;
    }
    .edit_event #uploadedimage1.img-responsive{
        margin-bottom: 10px;
    }
    .create_event #uploadedimage1{
        margin-top: 10px;
    }
    .edit_event #uploadedimage1, .create_event #uploadedimage1{
        width: 200px !important;
        max-width: 100%;
    }

    .price-row .text{
        max-height: 80px;
        overflow: hidden;
    }
    .services .text h2,  .services .text h3{
        font-size: 14px !important;
    }
    .services .text h2,  .services .text h3{
        margin:0 0 10px 0;
    }
    .skybox-adv>div {
        padding-bottom: 10px;
    }
    .skybox-adv img {
        max-height: 60px;
    }
    .skybox-adv .form-group{
        margin-top: 20px;
    }
    .skybox-adv div{
        line-height: 60px;
    }
    .skybox-adv .btn-group{
        /*line-height: 60px;*/
        margin: 19px 0;
    }
    .tr-img{
        max-width: 150px;
    }
    .hint input[type='file']{
        margin-bottom: 20px;
    }

    .schema #uploadedimage1 {
        max-width: 100%;
    }
    .schema label{
        text-align: center;
        display: block;
    }
    .schema:last-of-type{
        vertical-align: bottom;
        margin-top: 520px;
        text-align: right;

    }
    .objects .title{
        margin-top: 35px;
    }
    .objects .btns{
        margin-top: 30px;
    }

    .services img{
        width: 150px;
    }
    .services .text {
        /*height: 100px;
        overflow: hidden;*/
    }
    .services .text h3{
        margin: 0;
        font-size: 14px;
    }
    .point-row img {
        height: 150px;
        max-width:100%;
    }
    .point-row>div:nth-of-type(2) {
        line-height: 150px;
    }
    .point-row>.toggle-modern, .point-row>.btn-group {
        margin: 60px 0;
    }
    .schema img {
        max-height: 450px;
        margin: 0 auto;
        padding-bottom: 5px;
    }
    .point-row>div:nth-of-type(1) {
        line-height: 150px;
    }
    .dt-buttons.btn-group{
        display: none;
    }
    input[name="is_important"],input[name="is_adv"]{
        vertical-align: text-bottom;
    }
    .edit_event .is_important {
        line-height: 74px;
    }
    .sidebar-menu1 {
        white-space: nowrap;
        overflow: hidden;
    }
    .sidebar-menu1 {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    #cke_1_contents{
        /*height: 400px !important;*/
    }
    .rink_event-row{
        padding-bottom: 10px;
    }
    .sidebar {
        padding-bottom: 0px;
    }
    .sidebar .sidebar-menu .active .treeview-menu {
        display: block;
    }
    .schema{
        padding-bottom: 20px;
        margin-bottom: 50px;
    }
    .schema a{
        display: block;
        border: 1px solid #ccc;
        margin-bottom: 10px;
    }
    .schema a:hover{
        border: 1px solid #000;
    }
    .point-row{
        padding: 10px 0;
    }
    .tab-content{
        border: 1px solid #ccc;
    }
    .nav-tabs>li.active{
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
    }

    #uploadedimage_dep{
        display: none;
    }
    .gallery>div>div:last-of-type .img_dep {
        line-height: 1px;
        margin-left: 10px;
        margin-top: 115px;
    }
    .pr .btn-xs,.hall .btn-xs, .point-row .btn-xs, .rink_event-row .btn-xs{
        padding: 0px 10px;
        line-height: 30px;
        height: 30px;
    }
    .hall>div{
        padding-bottom: 10px;
    }
    .hall div.text{
        max-height: 60px;
        overflow: hidden;
    }
    .pr .toggle-modern {
        margin-bottom: 20px;
    }
    input[type=file]#img_dep {
        display: inline-block;
    }
    .gallery:not(.timetable)>div>div:nth-child(7n){
        clear: left;
    }

    .schedule .toggle-modern {
        margin-bottom: 5px;
    }
    .schedule .form-group {
        margin-bottom: 5px;
    }
    .gallery img{
        height: 100px;
        width: auto;
        /*max-width: 100%;*/
        margin: 0 auto;
    }
    .treeview-menu>li>a {
        padding: 8px 5px 8px 35px;
        display: block;
        font-size: 14px;
    }
    .treeview-menu{
        /*display:block;*/
    }
    fieldset{
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        /* -webkit-box-shadow: 0px 0px 0px 0px #000; */
        box-shadow: 0px 0px 0px 0px #ddd;
    }
    legend{
        width: inherit;
        padding: 0 10px;
        border-bottom: none;
    }
    #uploadedimage1{
        /* margin-bottom: 10px;*/
    }
    .schedule label{
        line-height: 34px;
        text-align: right;
        padding: 0;
    }
    .schedule input[type=checkbox]{
        vertical-align: text-bottom;
    }
    .schedule .toggle-modern .toggle-on{
        font-size: 10px;
    }
    .pdf,.pdf-btn{
        line-height: 34px;
    }
    .pdf-btn a:first-of-type {
        margin-right: 4px;
    }
    .pdf-btn a{
        border-top-left-radius: 3px !important;
        border-bottom-left-radius: 3px !important;
    }

    input[name="is_active"],input[name="day_off[]"],input[name="is_shown[]"] {
        display: none;
    }
    .toggle-modern{
        margin-bottom: 50px;
    }
    #uploadedimage1 {
        max-width: 40%;
    }
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    .inputfile + label {
        color: white;
        background-color: #3c8dbc;
        padding: 5px 10px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid #367fa9;
        font-weight: 400;
    }
    .inputfile:focus + label,
    .inputfile + label:hover {
        background-color: #2e6da4;

    }
    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
        margin-bottom: 0px;
        line-height: 18px;
        vertical-align: top;
    }
    .inputfile:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }
    .file-input{
        display: inline-block;
        float: left;
        margin-right: 5px;
    }



    .login-logo {
        font-size: 28px;
    }
    .dropdown.user.user-menu .pull-right {
        padding-top: 10px;
    }
    .main-header .navbar-custom-menu .navbar-nav {
        margin-right: 20px;
    }
    .jumbotron {
        background-color: #fff;
    }
    .main-header .logo .logo-lg {
        font-size: 13px;
    }
    .header{
        font-size: 22px;
        color: #fff;
        margin: 0 auto;
        padding: 5px 0;
        line-height: 40px;
    }
    .header img{
        max-width:50px;
        height:auto;
        padding-right: 5px;
    }
    .teams td>img{
        height: 66px;
    }
    .teams td:nth-of-type(2) {
        vertical-align: middle;
    }
    .dataTables_wrapper .dataTables_filter {
        float: left !important;
    }
    div.dataTables_wrapper div.dataTables_filter input {
        min-width: 200px;
    }
    .dataTables_wrapper {
        /*top:-30px;*/
    }
    .teams .dataTables_wrapper {
        top:-50px;
    }
    .teams h1, .places h1,.event_type h1,.event h1, .event #type,.event #hidePast{
        position: relative;
        z-index: 100;
    }
    .event .dataTable img{
        max-width:40px;
        display: inline-block;
        padding: 0 5px 0 0;
    }
    .event .dataTable img:nth-of-type(2) {
        margin-left: 30px;
    }
    .edit_event img{
        max-width: 250px;
    }
    .event .pull-left,.event .pull-right{
        margin-top: 20px;
        margin-bottom: 10px;
    }
    .event .checkbox-inline{
        line-height: 34px;
    }
    .event .checkbox-inline input{
        vertical-align: text-bottom;
        margin-right: 5px;
    }
    .create_event .league, .create_event .kind, .create_event .teams, .create_event .desc, .create_event .name{
        display: none;
    }
    .teams-btn{
        margin-left: 10px;
        margin-top: 5px;
    }
    .events-btn{
        margin-right: 10px;
        margin-top: 5px;
    }
    .teams-btn>a, .events-btn>a{
        /*font-size: 18px;*/
    }
    .content-header>h1{
        padding-bottom: 15px;
    }
    .dataTables_wrapper {
        overflow-x: auto;
    }
    .pull-left.header>a{
        color: #fff;
    }
    .dataTable .command{
        width: 49%;
        display: inline-block;
    }
    @media (max-width: 1720px) and  (min-width: 1200px) {

        .event .dataTable img{
            max-width:20px;
        }
    }
    @media (max-width: 1400px) {
        .schedule label{
            font-size: 11px;
        }
    }
    @media (max-width: 1200px) {
        .event .dataTable img{
            max-width:20px;
        }
        .event .dataTable img:nth-of-type(2) {
            margin-left: 5px;
        }
        .event>.pull-right{
            margin-top: 50px;
        }
        .filter>div:last-of-type{
            line-height: 60px;
        }
        .dataTable .command {
            width: 100%;
            display: block;
        }
        .schedule label{
            font-size: 10px;
        }
    }

    @media (max-width: 990px) {
        .filter > div {
            margin: 5px 0;
        }
    }

    @media (max-width: 991px){
        .main-header .navbar-custom-menu a.btn-default  {
            background-color: #f4f4f4;
        }
    }

</style>
