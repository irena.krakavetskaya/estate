@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="jumbotron">
                    <h2 class="display-3">Админка  FINERS</h2>

                    <h3>404</h3>
                    <h4>Страница не найдена</h4>
                    <a href="/">Назад</a>
                </div>
            </div>

        </div>
    </div>
@endsection