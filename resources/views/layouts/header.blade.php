<section class="content-header">

        <h1 class="pull-left">{{ $title }}</h1>

        <div class="pull-right">
            <a class="btn btn-primary btn-sm pull-right" style="" href="{!! url($link.'/browse/file') !!}">Загрузить файл в .xls, .csv</a>
        </div>

        {!! Form::open(['url' =>  [$link.'/download'], 'method' => 'post', 'class'=>'pull-right']) !!}
        {!! Form::button('Скачать в .xls', [
                'type' => 'submit',
                'class' => 'btn btn-danger btn-sm',
        ]) !!}
        {!! Form::close() !!}

    <div class="pull-right">
        <a class="btn btn-primary btn-sm pull-right" style="margin-bottom: 15px;" href="{!! route($link.'.create') !!}">	&#43; Добавить {{$add}}</a>
    </div>

</section>
