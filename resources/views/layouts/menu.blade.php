
<li class="{{ Request::is('cities*') ? 'active' : '' }}">
    <a href="{!! route('cities.index') !!}"><i class="fa fa-map-marker"></i><span>Города</span></a>
</li>

<li class="{{ Request::is('districts*') ? 'active' : '' }}">
    <a href="{!! route('districts.index') !!}"><i class="fa fa-edit"></i><span>Районы</span></a>
</li>

<li class="{{ Request::is('microDistricts*') ? 'active' : '' }}">
    <a href="{!! route('microDistricts.index') !!}"><i class="fa fa-edit"></i><span>Микрорайоны</span></a>
</li>

<li class="{{ Request::is('developers*') ? 'active' : '' }}">
    <a href="{!! route('developers.index') !!}"><i class="fa fa-edit"></i><span>Застройщики</span></a>
</li>

<li class="{{ Request::is('dealTypes*') ? 'active' : '' }}">
    <a href="{!! route('dealTypes.index') !!}"><i class="fa fa-edit"></i><span>Тип сделки</span></a>
</li>

<li class="{{ Request::is('objectTypes*') ? 'active' : '' }}">
    <a href="{!! route('objectTypes.index') !!}"><i class="fa fa-edit"></i><span>Тип объекта</span></a>
</li>


<li class="{{ Request::is('objects*') ? 'active' : '' }}">
    <a href="{!! route('objects.index') !!}"><i class="fa fa-edit"></i><span>Объект</span></a>
</li>

<br>
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Пользователи</span></a>
</li>
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Роли</span></a>
</li>

<li class="{{ Request::is('logs*') ? 'active' : '' }}">
    <a href="{!! route('logs.index') !!}"><i class="fa fa-edit"></i><span>Logs</span></a>
</li>



