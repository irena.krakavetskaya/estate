<script>

    //color of status
    let obj = $('.status>select');
    $.each( obj, function( key, value ) {
        //console.log($(value).val());
        if($(value).val()==1){
            $(this).addClass('active');
        }
        else{
            $(this).addClass('inactive');
        }
    });
    $('.status').on('change', 'select', function(){
            $(this).toggleClass('active').toggleClass('inactive');
    });
    //end color of status

    $('.reset-rates').click(function(){
        event.preventDefault();
        let month = $('.investment_rates .month');
        $.each( month, function( key, value ) {
            $(value).val('');
        });
    });


    //preloader for departments upload, upload1
    $('form').one('click', '#save_departments', function (event) {
        event.preventDefault();
        showPreloader($(this), 'Происходит загрузка данных, пожалуйста подождите...',280000);
        /*
         $.blockUI({
             message: "<h3>Происходит загрузка данных, пожалуйста подождите...</h3>",
             timeout: 35000, //19000
             onUnBlock:$(this).parents("form").submit()
         });
         */
    });


    //preloader for departments download, download1
    $('form').one('click', '#download_departments', function (event) {
        event.preventDefault();
        showPreloader($(this), 'Происходит выгрузка данных, пожалуйста подождите...',180000);
    });

    $('form').one('click', '#download1_departments', function (event) {
        event.preventDefault();
        showPreloader($(this), 'Происходит выгрузка данных, пожалуйста подождите...',180000);
    });

    //preloader for departments fill coords
    $('form').one('click', '#fill_coords', function (event) {
        event.preventDefault();
        showPreloader($(this), 'Происходит заполнение координат, пожалуйста подождите...',80000);
    });

    //preloader for departments check coords
    $('form').one('click', '#check_coords', function (event) {
        event.preventDefault();
        showPreloader($(this), 'Происходит проверка координат, пожалуйста подождите...',80000);
    });

    function showPreloader($elem, $message,$time){
        $.blockUI({
            message: "<h3>"+$message+"</h3>",
            timeout: $time,
            onUnBlock:$elem.parents("form").submit()
        });
    }
    //end of preloader

    //checkbox
    function changeCheckbox(name){
        if($('input[name="'+name+'"]').val()==='0'){
            $('input[name="'+name+'"]').prop('checked','');
        }
        $('input[name="'+name+'"]').click(function(){
            if($(this).prop('checked')===true){
                $(this).val('1');
            }
            else{
                $(this).val('0');
            }
        });
    }

    changeCheckbox('activity');
    changeCheckbox('insurance_status');
    changeCheckbox('end_of_term_status');
    changeCheckbox('monthly_payment_status');
    changeCheckbox('cancellation_status');
    changeCheckbox('capitalization_status');
    changeCheckbox('refill_status');
    changeCheckbox('partial_withdrawal_status');
    changeCheckbox('for_pensioners_status');
    //end of checkbox



    //divide into blocks on investment page
    if($('.dataTable').length>0){

        function initHidden($class){
            if($('.id').length>1){
                //console.log($class);
                table.columns().visible( false );
                table.columns('.id').visible( true );
                table.columns('.actions').visible( true );
                table.columns('.'+$class).visible( true );
            }
        }
        function getHidden(){
            var id = $('.changeVisibility.btn-primary').attr('id');
            //console.log(id);
            initHidden(id);
        }

        //format % in dataTable investment_rates
        function getFormat(){
            var obj = $(".investment_rates td").not(":has(a)").wrapInner("<a/>");//$(".investment_rates td").not().has(">a");//$(".investment_rates td").not(".actions");
            $.each(obj, function( key, value ) {
                str= $(value).text().replace(".", ",");
                $(this).text(str);
            });

            /*
            table.columns(7).data()
                .map( function ( value, index ) { //each

                    if(value.length>0){
                        console.log( value );
                        value.splice(index, 1000, 99 );
                        console.log( value );
                    }

                });
                */

        }

        const table = window.LaravelDataTables['dataTableBuilder'];
        table.on('init', function () {
                $('.changeVisibility').on('click', function () {
                    $('.changeVisibility.btn-primary').removeClass('btn-primary').addClass('btn-default');
                    $(this).addClass('btn-primary');
                    $(this).removeClass('btn-default');
                    var id = $(this).attr('id');
                    initHidden(id);
                    //console.log(id);
                });
                //divider in datatables rates
                /*
                if($('a.divider-line').length>0){
                    $('a.divider-line').parent().css('border-top', '1px solid #111');//.addClass('divider');
                    $('a.divider-line').parent().siblings().css('border-top', '1px solid #111');
                }
                */
                let currency = $('.divider-line');
                let previous = -1;
                $.each(currency, function( key, value ) {
                    if(previous !== -1){
                        console.log($(currency[previous]).html());
                        if($(currency[previous]).html() !== $(currency[key]).html()){
                            console.log(true);
                            $(this).parent().css('border-top', '1px solid #111');
                            $(this).parent().siblings().css("border-top", "1px solid #111");
                        }
                    }
                    previous = key;
                });

        });

        table
            .on( 'order.dt',  function () {
                getHidden();
                getFormat();
            })
            .on( 'search.dt', function () {
                getHidden();
                getFormat();
            })
            .on( 'page.dt',   function () {
                getHidden();
                getFormat();
            });
    }

    //end of dividing into blocks on investment page

    $('#dataTableBuilder_wrapper').on('click', '.paginate_button ', function () {
        //console.log(true);
    });



    //visual editor for fields
    if($('.ckeditor1').length>0){
        CKEDITOR.replace('html');
    }
    //end of visual editor for fields


    function addColor(val){
         if(val == 'customer'){
             $('input[name="color"]').show().attr('required','required');
             $('.color').show();
             var color = $('input[name="color"]').val();
             $('.color').css('background',color );

             $('input[name="color"]').mouseout(function(){ //focus, chnage blur mousemove
                 var color = $('input[name="color"]').val();
                 $('.color').css('background',color );
             });
         }
         else{
             $('input[name="color"]').hide().removeAttr('required');
             $('.color').hide();
         }
    }
    if($('.deposit').length>0){
        addColor($('input[name="color1[]"]:checked').val());
        $('input[name="color1[]"]').click(function(){
            addColor($(this).val());
        });
    }


    //lightbox gallery
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });


    //preview img gallery museum
    if($("#img_dep").length>0) {
        document.getElementById("img_dep").onchange = function () {
            showPreview(this);
        };
    }

    function showPreview(elem) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (e.total > 3000000) {//1mb - 1000 kb - 1 000 000 byte
                $('#imageerror_dep').text('Размер изображения не должен превышать 3 мб');
                $jimage = $(".img_dep").last();
                $jimage.val("");
                $jimage.wrap('<form>').closest('form').get(0).reset();
                $jimage.unwrap();
                $('#uploadedimage_dep').removeAttr('src');
                return;
            }
            $('#imageerror_dep').text('');
            $('.uploadedimage_dep').last().attr('src',e.target.result);
            $(".uploadedimage_dep").last().css('margin-bottom', '10px');

            //new
            var id_img = $('.delete-img').last().attr('id');
            id_img = ++id_img;
            var classBoot= 'col-sm-3';
            var str='<div class="'+classBoot+'"><input type="hidden" name="MAX_UPLOAD_SIZE" value="3000"><img id="uploadedimage_dep" height="160px" class="uploadedimage_dep"/>' +
                '<span id="imageerror_dep" style="font-weight: bold; color: red" >' +
                '</span><input id="img_dep" accept="image/jpg,image/jpeg,image/png" name="img_dep[]" type="file" class="img_dep"></div>';
            var str1='<p class="text-center"><input name="del_img[]" type="hidden" value="' + id_img + '" class="del_img">'+
                '<a href="#" class="btn btn-danger btn-sm delete-img" id="' + id_img + '">Удалить </a></p>';

            $('.uploadedimage_dep').last().show();
            $('.img_dep').last().hide();
            $('.img_dep').last().parent().after(str);
            $('.img_dep').eq(-2).after(str1);
            deleteImgMuseum();

            var last = $(".img_dep").last();
            last.on('change', function(){
                showPreview(last[0]);
            });
        };
        reader.readAsDataURL(elem.files[0]);
    }

    //delete img gallery museum
    deleteImgMuseum();
    function deleteImgMuseum(){
        $('.delete-img').click(function () {
            event.preventDefault();
            $(this).parent().parent().remove();
        });
    }
    //end of delete and preview img gallery museum


    if($("#img").length>0) {
        document.getElementById("img").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (e.total > 10000000) {//1mb - 1000 kb - 1 000 000 byte
                    $('#imageerror1').text('Размер изображения не должен превышать 10 мб');
                    $jimage = $("#img");
                    $jimage.val("");
                    $jimage.wrap('<form>').closest('form').get(0).reset();
                    $jimage.unwrap();
                    $('#uploadedimage1').removeAttr('src');
                    return;
                }
                $('#imageerror1').text('');
                document.getElementById("uploadedimage1").src = e.target.result;
            };
            reader.readAsDataURL(this.files[0]);
        };
    }

    /*reset filter*/
    if(window.location.pathname ==='/investmentRates' || window.location.pathname ==='/investments'){
        if(window.location.search.length>0){
            $('#filter').collapse('show');
        }
    }
    /*end of reset filter*/




</script>