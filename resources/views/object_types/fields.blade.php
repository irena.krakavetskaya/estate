<!-- Object Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('object_type', 'Object Type:') !!}
    {!! Form::text('object_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('objectTypes.index') !!}" class="btn btn-default">Отмена</a>
</div>
