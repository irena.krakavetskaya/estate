@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Типы объекта</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary btn-sm pull-right" style="" href="{!! route('objectTypes.create') !!}">&#43; Добавить новый тип объекта</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('object_types.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

