<!-- Micro District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('micro_district_id', 'Микрорайон:') !!}
    @isset($object['micro_district_id'])
        {!!Form::select('micro_district_id', $microdistricts, $object['micro_district_id'], ['class' => 'form-control'])!!}
    @else
        {!!Form::select('micro_district_id', $microdistricts, null, ['class' => 'form-control'])!!}
    @endisset
</div>

<!-- Deal Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deal_type_id', 'Тип сделки:') !!}
    @isset($object['deal_type_id'])
        {!!Form::select('deal_type_id', $dealTypes, $object['deal_type_id'], ['class' => 'form-control'])!!}
    @else
        {!!Form::select('deal_type_id', $dealTypes, null, ['class' => 'form-control'])!!}
    @endisset
</div>

<!-- Object Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('object_type_id', 'Тип объекта:') !!}
    @isset($object['object_type_id'])
        {!!Form::select('object_type_id', $objectTypes, $object['object_type_id'], ['class' => 'form-control'])!!}
    @else
        {!!Form::select('object_type_id', $objectTypes, null, ['class' => 'form-control'])!!}
    @endisset
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Of Storeys Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_of_storeys', 'Этажность:') !!}
    {!! Form::number('number_of_storeys', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Тип дома:') !!}
    @isset($object['type'])
        {!!Form::select('type', $types, $object['type'], ['class' => 'form-control'])!!}
    @else
        {!!Form::select('type', $types, null, ['class' => 'form-control'])!!}
    @endisset
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('objects.index') !!}" class="btn btn-default">Отмена</a>
</div>
