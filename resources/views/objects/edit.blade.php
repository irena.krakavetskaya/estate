@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Объекты
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($object, ['route' => ['objects.update', $object->id], 'method' => 'patch']) !!}

                        @include('objects.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection