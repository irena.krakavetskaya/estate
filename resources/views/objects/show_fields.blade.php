<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $object->id !!}</p>
</div>

<!-- Micro District Id Field -->
<div class="form-group">
    {!! Form::label('micro_district_id', 'Micro District Id:') !!}
    <p>{!! $object->micro_district_id !!}</p>
</div>

<!-- Deal Type Id Field -->
<div class="form-group">
    {!! Form::label('deal_type_id', 'Deal Type Id:') !!}
    <p>{!! $object->deal_type_id !!}</p>
</div>

<!-- Object Type Id Field -->
<div class="form-group">
    {!! Form::label('object_type_id', 'Object Type Id:') !!}
    <p>{!! $object->object_type_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $object->name !!}</p>
</div>

<!-- Number Of Storeys Field -->
<div class="form-group">
    {!! Form::label('number_of_storeys', 'Number Of Storeys:') !!}
    <p>{!! $object->number_of_storeys !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $object->type !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $object->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $object->updated_at !!}</p>
</div>

