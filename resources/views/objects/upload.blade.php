@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Загрузка файла с городами
        </h1>
    </section>
    <div class="content">

        <!--include('adminlte-templates::common.errors')-->

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['url' => 'cities/upload/file', 'files'=>true,'enctype'=>'multipart/form-data']) !!}

                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('cities', 'Файл:') !!}
                        {!! Form::file('cities', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-sm-12">
                        {!! Form::submit('Загрузить', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('cities.index') !!}" class="btn btn-default">Отмена</a>
                    </div>
                    {!! Form::close() !!}

                    <div class="col-sm-12">
                        <p>* Для загрузки необходимо сохранить лист Excel как csv-файл: "Файл"->"Скачать как"->"csv-файл (текущий лист)"</p>
                        <p>* При повторной загрузке csv-файла изменения сделанные в админ-панели не сохраняться</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection