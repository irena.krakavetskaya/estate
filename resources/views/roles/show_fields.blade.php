<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $city->id !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $city->city !!}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', 'City Id:') !!}
    <p>{!! $city->city_id !!}</p>
</div>

<!-- Region Field -->
<div class="form-group">
    {!! Form::label('region', 'Region:') !!}
    <p>{!! $city->region !!}</p>
</div>

<!-- Translit Field -->
<div class="form-group">
    {!! Form::label('translit', 'Translit:') !!}
    <p>{!! $city->translit !!}</p>
</div>

<!-- Roditelniy Field -->
<div class="form-group">
    {!! Form::label('roditelniy', 'Roditelniy:') !!}
    <p>{!! $city->roditelniy !!}</p>
</div>

<!-- Predlozhniy Field -->
<div class="form-group">
    {!! Form::label('predlozhniy', 'Predlozhniy:') !!}
    <p>{!! $city->predlozhniy !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $city->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $city->updated_at !!}</p>
</div>

