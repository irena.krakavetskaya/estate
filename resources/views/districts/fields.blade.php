<!-- City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_id', 'Город:') !!}
    @isset($district['city_id'])
        {!!Form::select('city_id', $cities, $district['city_id'], ['class' => 'form-control'])!!}
    @else
        {!!Form::select('city_id', $cities, null, ['class' => 'form-control'])!!}
    @endisset
</div>


<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('district_id', 'District Id:') !!}
    {!! Form::number('district_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Translit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('translit', 'Translit:') !!}
    {!! Form::text('translit', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('districts.index') !!}" class="btn btn-default">Отмена</a>
</div>
