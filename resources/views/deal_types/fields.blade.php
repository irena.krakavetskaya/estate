<!-- Deal Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deal_type', 'Тип сделки:') !!}
    {!! Form::text('deal_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('dealTypes.index') !!}" class="btn btn-default">Отмена</a>
</div>
