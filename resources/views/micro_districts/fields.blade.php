<!-- District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('district', 'Район:') !!}
    @isset($microdistrict['city_id'])
        {!!Form::select('district_id', $districts, $microdistrict['district_id'], ['class' => 'form-control'])!!}
    @else
        {!!Form::select('district_id', $districts, null, ['class' => 'form-control'])!!}
    @endisset
</div>

<!-- Micro District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('micro_district_id', 'Micro District Id:') !!}
    {!! Form::number('micro_district_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Translit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('translit', 'Translit:') !!}
    {!! Form::text('translit', null, ['class' => 'form-control']) !!}
</div>

<!-- Roditelniy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('roditelniy', 'Roditelniy:') !!}
    {!! Form::text('roditelniy', null, ['class' => 'form-control']) !!}
</div>

<!-- Predlozhniy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('predlozhniy', 'Predlozhniy:') !!}
    {!! Form::text('predlozhniy', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('microDistricts.index') !!}" class="btn btn-default">Отмена</a>
</div>
