@extends('layouts.app')

@section('content')

    @include('layouts.header', ['title' => 'Микрорайоны','link' => 'microDistricts','add'=>'микрорайон'])

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('micro_districts.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

