@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Микрорайоны
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($microDistrict, ['route' => ['microDistricts.update', $microDistrict->id], 'method' => 'patch']) !!}

                        @include('micro_districts.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection