<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $microDistrict->id !!}</p>
</div>

<!-- District Id Field -->
<div class="form-group">
    {!! Form::label('district_id', 'District Id:') !!}
    <p>{!! $microDistrict->district_id !!}</p>
</div>

<!-- Micro District Id Field -->
<div class="form-group">
    {!! Form::label('micro_district_id', 'Micro District Id:') !!}
    <p>{!! $microDistrict->micro_district_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $microDistrict->name !!}</p>
</div>

<!-- Translit Field -->
<div class="form-group">
    {!! Form::label('translit', 'Translit:') !!}
    <p>{!! $microDistrict->translit !!}</p>
</div>

<!-- Roditelniy Field -->
<div class="form-group">
    {!! Form::label('roditelniy', 'Roditelniy:') !!}
    <p>{!! $microDistrict->roditelniy !!}</p>
</div>

<!-- Predlozhniy Field -->
<div class="form-group">
    {!! Form::label('predlozhniy', 'Predlozhniy:') !!}
    <p>{!! $microDistrict->predlozhniy !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $microDistrict->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $microDistrict->updated_at !!}</p>
</div>

