<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $developer->id !!}</p>
</div>

<!-- Dev Id Field -->
<div class="form-group">
    {!! Form::label('dev_id', 'Dev Id:') !!}
    <p>{!! $developer->dev_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $developer->name !!}</p>
</div>

<!-- Legal Field -->
<div class="form-group">
    {!! Form::label('legal', 'Legal:') !!}
    <p>{!! $developer->legal !!}</p>
</div>

<!-- Html Field -->
<div class="form-group">
    {!! Form::label('html', 'Html:') !!}
    <p>{!! $developer->html !!}</p>
</div>

<!-- Web Field -->
<div class="form-group">
    {!! Form::label('web', 'Web:') !!}
    <p>{!! $developer->web !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $developer->email !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $developer->address !!}</p>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{!! $developer->logo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $developer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $developer->updated_at !!}</p>
</div>

