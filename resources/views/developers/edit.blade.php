@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Застройщик
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($developer, ['route' => ['developers.update', $developer->id], 'method' => 'patch']) !!}

                        @include('developers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection