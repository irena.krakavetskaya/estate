<!-- Dev Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dev_id', 'Dev Id:') !!}
    {!! Form::number('dev_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Legal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('legal', 'Юр. название:') !!}
    {!! Form::text('legal', null, ['class' => 'form-control']) !!}
</div>

<!-- Html Field -->
<div class="form-group col-sm-6">
    {!! Form::label('html', 'Html описания:') !!}
    {!! Form::textarea('html', null, ['class' => 'form-control ckeditor1']) !!}
</div>

<!-- Web Field -->
<div class="form-group col-sm-6">
    {!! Form::label('web', 'Сайт:') !!}
    {!! Form::text('web', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Эл. почта:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Адрес:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Логотип:') !!}
    {!! Form::text('logo', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('developers.index') !!}" class="btn btn-default">Отмена</a>
</div>
