<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Дата и время:') !!}
    <p>{!! $log->created_at !!}</p>
</div>

<!-- Log Name Field -->
<div class="form-group">
    {!! Form::label('log_name', 'Тип изменения:') !!}
    <p>{!! $log->log_name !!}</p>
</div>

<!-- Subject Type Field -->
<div class="form-group">
    {!! Form::label('subject_type', 'Тип субъекта изменения:') !!}
    <p>{!! $result['subject'] !!}</p>
</div>

<!-- Subject Id Field -->
<div class="form-group">
    {!! Form::label('subject_id', 'ID субъекта:') !!}
    <p><a href="/{!! $result['subject_table'] !!}/{!! $log->subject_id !!}/edit" target="_blank">{!! $log->subject_id !!}</a>
</div>


<!-- Causer Id Field -->
<div class="form-group">
    {!! Form::label('causer_id', 'Кто изменил:') !!}
    <p><a href="/users/{!! $log->causer_id !!}/edit" target="_blank">{!! $result['email'] !!}</a></p>
</div>


@isset($result['prop'])
<div class="form-group">
    {!! Form::label('properties', 'Изменения:') !!}

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td><strong>Свойство</strong></td>
            <td><strong>Старое значение</strong></td>
            <td><strong>Новое значение</strong></td>
        </tr>
        </thead>
        <tbody>
        @foreach($result['prop'] as $key=>$val)
           <tr>
                <td>{!! $key !!}</td>
                <td>
                       @if($log->log_name=='update')
                           {!! $val[0] !!}
                       @elseif($log->log_name=='delete')
                           {!! $val[1] !!}
                       @endif
                </td>
                <td>
                    @if($log->log_name=='update' || $log->log_name=='create')
                        {!! $val[1] !!}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endisset


